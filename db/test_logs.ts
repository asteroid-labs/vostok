const debug = false
import { pool } from './index';
import { test_log } from '../interfaces/index';

export async function create(test_log: test_log) {
    let res = {} as any
    let text = 'INSERT INTO test_logs (time, test_time, id, text, type) '+
                'VALUES ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING RETURNING ID;'
    let values = [
        Number(Date.now()),
        test_log.test_time,
        test_log.id,
        test_log.text,
        test_log.type
    ]
        try {
            res = await pool.query(text, values) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save test_log')
            }
        } catch (err) {
            console.log(err.stack, 'error test_log')
        }
    return res.rows[0] as test_log
}

export async function unlinkAll(id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM test_logs WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error delete test_log', id)
    }
    return true
}

export async function getAll(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM test_logs WHERE id = $1 ORDER BY time DESC;', [id])
    } catch (err) {
        console.log(err.stack, 'error get test_log')
    }
    return res.rows as test_log[]
}

export async function getLatest(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM test_logs WHERE id = $1 ORDER BY time DESC LIMIT 1;', [id])
    } catch (err) {
        console.log(err.stack, 'error getLatest  test_log')
    }
    return res.rows[0] as test_log
}

export async function unlinkNewer(id: number, time: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM test_logs WHERE id = $1 AND test_time > $2;', [id, time])
    } catch (err) {
        console.log(err.stack, 'error deleteNewer test_log', id)
    }
    return true
}