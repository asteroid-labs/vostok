#!/bin/bash
#docker run -d --name tdb-vostok-beta -p 5435:5432 -e POSTGRES_PASSWORD=S3cret --restart=unless-stopped timescale/timescaledb:latest-pg11
echo "**** HAVE YOU FORGOT TO COPY THE CONFIG AND CERTS ?? ****"
docker stop vostok-beta
docker rm vostok-beta
docker build -t vostok-beta .
docker run -d --name vostok -p 4000:4000 --network="host" --restart=unless-stopped -v /build/backtest/results vostok-beta
docker start vostok-beta
