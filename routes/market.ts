import express from 'express';
import * as db_market from '../db/market';
import * as config from '../config.json';
import { user, market } from '../interfaces/index';

let router: express.Router = express.Router();

router.get('', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            let admin = true
            let markets = await db_market.getAll()
            res.render('pages/markets.ejs', {auth, admin, markets })
        } else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
})

router.post('/:market_id', async function(req, res) {
    if (req.isAuthenticated()) {
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            const market_id = Number(req.params.market_id)
            
            if (req.body.delete_market === '') {
                let del = await db_market.unlink(market_id)
                res.redirect('back')
            } else {
                // save market's config
                let market = {
                    id: market_id,
                    name: req.body.name,
                    sputnik_id: Number(req.body.sputnik_id)
                } as market
                await db_market.update(market)
                res.redirect('/market/' + market_id)
            }
        } else {
            // not logged in
            res.redirect('/login')
        }
    } else {
        // not logged in
        res.redirect('/login')
    }
})

router.get('/new', async function(req, res) {
    if (req.isAuthenticated() && req.user) {
        const user = req.user as user
            if (user.email.toLowerCase() === config.web.email) {
                let market = {
                    name: 'market name',
                    sputnik_id: 0,
                } as market
                let tf = await db_market.create(market)
                res.redirect('/market/' + tf.id)
            } else {
                // not logged in
                res.redirect('/login');
            }
    } else {
        res.redirect('/login');
    }
})

router.get('/:market_id', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
            if (user.email.toLowerCase() === config.web.email) {
                let admin = true
                const market_id = Number(req.params.market_id)
                let markets = await db_market.getOne(market_id)
                res.render('pages/markets.ejs', {auth, admin, markets })
            } else {
                // not logged in
                res.redirect('/login');
            }
    } else {
        res.redirect('/login');
    }
})

export = router
