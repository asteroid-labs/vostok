import { complete_test_info } from '../../interfaces/index';
import { MarketBuyLayer, MarketSellLayer } from './market';
import { LimitBuyLayer, LimitSellLayer } from './limit';
import { stall } from '../tools/stall';
import { write } from '../tools/logs';
const debug = false

// looping buy layers
export async function loopBuyLayers(test: complete_test_info, trigger: boolean){
    // looping layers
    for(const [i, layer] of test.algo.buy_pref.layers.entries()){
        test.algo.buy_pref.layer_index = i
        if(layer.stall_protect.active){
            test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall_protect.current = layer.stall_protect.current - 1 
            write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                'stall',
                test.original.id,
                debug,
                `: buy Stall Protect is active and reduced to ${layer.stall_protect.current} minutes.`
            )
        }
        switch (layer.type) {
            case 'market':
                test = await stall(test, 'buy', trigger, MarketBuyLayer);
                break;
        
            case 'limit':
                test = await stall(test, 'buy', trigger, LimitBuyLayer);
                break;
        } 
    }
    return test
}

// looping sell layers
export async function loopSellLayers(test: complete_test_info, trigger: boolean){
    // looping layers
    for(const [i, layer] of test.algo.sell_pref.layers.entries()){
        test.algo.sell_pref.layer_index = i
        if(layer.stall_protect.active){
            test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall_protect.current = layer.stall_protect.current - 1 
            write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                'stall',
                test.original.id,
                debug,
                `: sell Stall Protect is active and reduced to ${layer.stall_protect.current} minutes.`
            )
        }
        switch (layer.type) {
            case 'market':
                test = await stall(test, 'sell', trigger, MarketSellLayer);
                break;
        
            case 'limit':
                test = await stall(test, 'sell', trigger, LimitSellLayer);
                break;
        } 
    }
    return test
}
