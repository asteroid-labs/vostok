import { message_info } from '../interfaces/index';
import * as db_user from '../db/user';
import { write } from '../trading/tools/logs';
import * as config from '../config.json';
import Telegraf from 'telegraf';
const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')

const bot = new Telegraf(config.telegram.token)

const debug = false

function capitalizeFirstLetter(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

export async function sendTelegramTrade(user_id: number, type: string, account_name: string, market: message_info){
    let user = await db_user.getUser(user_id)
    if(type.length > 0){
        type = type.charAt(0).toUpperCase() + type.slice(1);
    }
    let message = ''
    if(market.side === 'buy'){
        message = `--- Binance / ${account_name} ---\n${type} ${capitalizeFirstLetter(market.side)} trade recorded ✅\nPrice: ${market.price.toFixed(8)} ${market.base}\nQty: ${market.qty.toFixed(5)} ${market.quote} / ${(market.qty*market.price).toFixed(5)} ${market.base}`
    } else{
       message = `--- Binance / ${account_name} ---\n${type} ${capitalizeFirstLetter(market.side)} trade recorded ✅\nPrice: ${market.price.toFixed(8)} ${market.base}\nQty: ${market.qty.toFixed(5)} ${market.quote} / ${(market.qty*market.price).toFixed(5)} ${market.base}\nGain: ${market.gain.toFixed(5)} / ${market.gain_percent.toFixed(3)}%`
    }
    try{
        await sendTelegramMsg(user.prefs.telegram_id, message)
    }catch(err){
        console.log(err, 'sending telegram msg')
    }
    
}

export async function sendTelegramMsg(user_telegram_id: number, message: string){

    try{
        await bot.telegram.sendMessage(user_telegram_id, message)
    }catch(err){ 
        write(0, '', 'error', 0, debug, `: trade error # ${err}`)
        
    }
    
}