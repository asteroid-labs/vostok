import * as db_test from '../db/tests';
import child from 'child_process';

const debug = true

async function main() {
    let tests = await db_test.getDanglings()

    // fork backtest with backtest.id
    if(tests.length > 0){
        for(const test of tests){
            try {
                let forked = child.fork(`./build/trading/${test.type}/index.js`)
                forked.on('message', (msg) => {
                    console.log(`Message from ${test.type}er: "${msg}"`)
                    })
                forked.send({ test_id : test.id })
                debug ? console.log(`Started dangling ${test.type} #`, test.id, test.state) : false
            } catch (err) {
                console.log(err.stack, `forking ${test.type}`)
            }
        }
    }
}

main()