import express from 'express';
import * as db_indicator from '../db/indicator';
import * as config from '../config.json';
import { user, indicator } from '../interfaces/index';

let router: express.Router = express.Router();

router.get('', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            let admin = true
            let indicators = await db_indicator.getAll()
            res.render('pages/indicators.ejs', {auth, indicators })
        } else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
})


router.post('/:indicator_id',async function(req, res) {
    if (req.isAuthenticated()) {
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
        const indicator_id = Number(req.params.indicator_id)
        
        if (req.body.delete_indicator === '') {
            let del = await db_indicator.unlink(indicator_id)
            res.redirect('back')
        } else {
            // save indicator's config
            let periods = req.body.periods.split(',')
            let indicator = {
                id: indicator_id,
                name: req.body.name,
                function: req.body.function,
                periods: periods
            } as indicator
            await db_indicator.update(indicator)
            res.redirect('/indicator/' + indicator_id)
        }
        } else {
            // not logged in
            res.redirect('/login')
        }
    } else {
        // not logged in
        res.redirect('/login')
    }
})

router.get('/new', async function(req, res) {
    if (req.isAuthenticated() && req.user) {
        const user = req.user as user
            if (user.email.toLowerCase() === config.web.email) {
                let indicator = {
                    name: 'Indicator',
                    function: 'tulip indicator',
                    periods: []
                } as indicator
                let tf = await db_indicator.create(indicator)
                res.redirect('/indicator/' + tf.id)
            } else {
                // not logged in
                res.redirect('/login');
            }
    } else {
        res.redirect('/login');
    }
})

router.get('/:indicator_id', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
            if (user.email.toLowerCase() === config.web.email) {
                let admin = true
                const indicator_id = Number(req.params.indicator_id)
                let indicators = [await db_indicator.getOne(indicator_id)]
                res.render('pages/indicators.ejs', {auth, admin, indicators })
            } else {
                // not logged in
                res.redirect('/login');
            }
    } else {
        res.redirect('/login');
    }
})

export = router
