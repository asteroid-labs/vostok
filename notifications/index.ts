import { test_trade, complete_test_info, message_info, execution_notif, account, user } from '../interfaces/index';
import * as db_test_trade from '../db/test_trade';
import * as db_account from '../db/account';
import * as db_user from '../db/user';
import { sendMailTrade } from './email'
import { sendTelegramTrade, sendTelegramMsg } from './telegram'
import { fetchLastOtherSideTrade } from '../binance/index';
import * as db_test_balance from '../db/test_balance';
import * as db_real_balance from '../db/real_balance';

const debug = false

export async function setupTradeMsg(test: complete_test_info, side: string){
    let candle = {
        qty: test.markets[test.index].trade_info.last.qty,
        base_value: 0,
        quote: test.markets[test.index].market.name.split("/")[0],
        base: test.markets[test.index].market.name.split("/")[1],
        type: test.type,
        gain: 0,
        gain_percent: 0,
        test_id: test.original.id,
        side: side,
        price: test.markets[test.index].trade_info.last.price
    } as message_info

    let user_id = test.original.user_id
    let account_id = test.original.account_id
    let base_balance = 0
    let quote_balance = 0
    if(test.type === 'real'){
        base_balance = (await db_real_balance.getOne(account_id, candle.base)).free
        quote_balance = (await db_real_balance.getOne(account_id, candle.quote)).free
    }else{
        base_balance = (await db_test_balance.getOne(test.original.id, candle.base)).free
        quote_balance = (await db_test_balance.getOne(test.original.id, candle.quote)).free
    }
    let previous_trade = {} as test_trade
    let trade = {
        id: 0,
        test: candle.test_id, 
        type: test.type,
        pair_id: test.markets[test.index].market.id,
        time: test.markets[test.index].market_data[test.candle.i][0],
        base_value: candle.side === 'buy' ? test.markets[test.index].trade_info.last.qty * test.markets[test.index].trade_info.last.price : test.markets[test.index].trade_info.last.qty,
        qty: candle.qty,
        price: candle.price,
        side: candle.side,
        account_base_value: base_balance,
        account_quote_value : quote_balance
    } as test_trade
    if(test.original.type !== 'real-notif'){
        previous_trade = await db_test_trade.getLast(test.original.id, candle.side)
        let ntrade = await db_test_trade.create(trade)
        //console.log(ntrade)
        test.markets[test.index].trade_info.id = ntrade.id as number
    }

    //use previous_trade to calculate gains
    let prev_amount = 0
        
    if(typeof previous_trade?.side != 'undefined'){
        prev_amount = previous_trade.price
    }
    
    candle.gain_percent = prev_amount === 0 ? 0 : (candle.price/prev_amount) * 100
    candle.gain = candle.gain_percent === 0 ? 0 : ((candle.price * candle.qty)-(prev_amount * candle.qty))
    if(test.original.type !== 'real'){
        let account = await db_account.getOne(user_id, account_id)
        if(account.prefs){
            let prefs = account.prefs as any
            if(test.type !== 'backtest' && prefs.email_notifications === 'on'){
                try{
                    await sendMailTrade(user_id, account.name, candle)
                }catch(err){
                    console.log(err, 'error sending email notif')
                }
            }

            if(test.type !== 'backtest' && prefs.telegram_notifications === 'on'){
                try{
                    await sendTelegramTrade(user_id, test.type, account.name, candle)
                }catch(err){
                    console.log(err, 'error sending telegram notif')
                }
            }
        }
    }

    return test
}

export async function setupNotifMsg(test: complete_test_info, user: user , account: account, data: execution_notif){
    // create notif for new, partially filled, filled, cancel orders
    if(data.orderType.length > 0){
        data.orderType = data.orderType.charAt(0) + data.orderType.toLowerCase().slice(1);
    }
    if(data.side.length > 0){
        data.side = data.side.charAt(0) + data.side.toLowerCase().slice(1);
    }

    let base = test.markets[test.index].market.name.split("/")[1]
    let quote = test.markets[test.index].market.name.split("/")[0]

    let message = ''
    if(data.side === 'Buy'){
        switch (data.orderStatus) {
            case 'NEW':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was placed.\nPrice: ${parseFloat(data.price)}\nQty: ${data.quantity} ${quote} / ${(parseFloat(data.quantity)*parseFloat(data.price)).toFixed(5)} ${base}`
        
                break;
        
            case 'PARTIALLY_FILLED':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was partially filled 😧.\nPrice: ${parseFloat(data.lastTradePrice)}\nAcc Qty: ${parseFloat(data.accumulatedQuantity)} ${quote} / ${(parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)).toFixed(5)} ${base}`
        
                break;

            case 'FILLED':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was filled ✅.\nPrice: ${parseFloat(data.lastTradePrice)}\nTotal Qty: ${parseFloat(data.accumulatedQuantity)} ${quote} / ${(parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)).toFixed(5)} ${base}`
        
                break

            case 'CANCELED':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was cancelled ❌.\nPrice: ${parseFloat(data.lastTradePrice)}\nAcc Qty: ${parseFloat(data.accumulatedQuantity)} ${quote} / ${(parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)).toFixed(5)} ${base}`
        
                break

            default:
                console.log(data)
                break
        }
    }else{
        // find last order, from binance?
        let prev_order = await fetchLastOtherSideTrade(account, test.markets[test.index].market)

        let gain = 0
        let gain_percent = 0
        let prev_price = 0
        if(typeof prev_order !== 'undefined'){
            gain = ((parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)) - (prev_order.amount*prev_order.average))
            gain_percent = gain / (prev_order.amount*prev_order.average) * 100
            prev_price = prev_order.average
        }
        switch (data.orderStatus) {
            case 'NEW':
                if(typeof prev_order !== 'undefined'){
                    gain = ((parseFloat(data.quantity)*parseFloat(data.price)) - (prev_order.amount*prev_order.average))
                    gain_percent = gain / (prev_order.amount*prev_order.average) * 100
                }

                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was placed.\nBought Price: ${prev_price.toFixed(8)}\nPrice: ${parseFloat(data.price)}\nQty: ${data.quantity} ${quote} / ${(parseFloat(data.quantity)*parseFloat(data.price)).toFixed(5)} ${base}\nGains: ${gain_percent.toFixed(3)}% / ${gain.toFixed(5)} ${base}`
        
                break;
        
            case 'PARTIALLY_FILLED':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was partially filled 😧.\nBought Price: ${prev_price.toFixed(8)}\nPrice: ${parseFloat(data.lastTradePrice)}\nAcc Qty: ${parseFloat(data.accumulatedQuantity)} ${quote} / ${(parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)).toFixed(5)} ${base}\nGains: ${gain_percent.toFixed(3)}% / ${gain.toFixed(5)} ${base}`
        
                break;

            case 'FILLED':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was filled ✅.\nBought Price: ${prev_price.toFixed(8)}\nPrice: ${parseFloat(data.lastTradePrice)}\nTotal Qty: ${parseFloat(data.accumulatedQuantity)} ${quote} / ${(parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)).toFixed(5)} ${base}\nGains: ${gain_percent.toFixed(3)}% / ${gain.toFixed(5)} ${base}`
        
                break

            case 'CANCELED':
                message = `--- Binance / ${account.name} ---\n${data.side} ${data.orderType} for ${data.symbol} was cancelled ❌.\nBought Price: ${prev_price.toFixed(8)}\nPrice: ${parseFloat(data.lastTradePrice)}\nAcc Qty: ${parseFloat(data.accumulatedQuantity)} ${quote} / ${(parseFloat(data.accumulatedQuantity)*parseFloat(data.lastTradePrice)).toFixed(5)} ${base}\nGains: ${gain_percent.toFixed(3)}% / ${gain.toFixed(5)} ${base}`
        
                break

            default:
                console.log(data)
                break
        }
    }

    if(account.prefs){
        let prefs = account.prefs as any
        let user_prefs = user.prefs as any
       /*  if(test.type != 'backtest' && prefs.email_notifications === 'on'){
            try{
                await sendMailTrade(account.user_id, account.name, candle)
            }catch(err){
                console.log(err, 'error sending email notif')
            }
        } */

        if(prefs.telegram_notifications === 'on'){
            try{
                await sendTelegramMsg(user_prefs.telegram_id, message)
            }catch(err){
                console.log(err, 'error sending telegram notif')
            }
        }
    }
}

export async function sendTestEnded(user_id: number, account_id: number, test_type: string, test_id: number){
    let account = await db_account.getOne(user_id, account_id)
    let user = await db_user.getUser(user_id)
    if(account.prefs){
        let prefs = account.prefs as any
        let user_prefs = user.prefs
        
       /*  if(test.type != 'backtest' && prefs.email_notifications === 'on'){
            try{
                await sendMailTrade(account.user_id, account.name, candle)
            }catch(err){
                console.log(err, 'error sending email notif')
            }
        } */

        if(prefs.telegram_notifications === 'on'){
            let type = ''
            switch (test_type) {
                case 'backtest':
                    type = 'Backtest'
                    break;
            
                case 'livetest':
                    type = 'Livetest'
                    break;
                case 'real':
                    type = 'Real Trading'
                    break;
            }
            let message = `--- Binance / ${account.name} ---\nA ${type} has ended, check out the results!`
            // implement link using @bot[name](link)
            try{
                await sendTelegramMsg(user_prefs.telegram_id, message)
            }catch(err){
                console.log(err, 'error sending telegram notif')
            }
        }
    }
}