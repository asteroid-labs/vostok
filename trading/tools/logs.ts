import * as db_test_logs from '../../db/test_logs'

export async function write(candle_date: number, market: string, type: string, id: number, debug = false, text:string){
    let ntext = `${date(candle_date)} ${market} ${text}`
    if(debug){
        console.log(ntext)
    }

    let test_log = {
        time: 0, 
        test_time: candle_date,
        id: id, 
        text: ntext,
        type: type,
    }
    db_test_logs.create(test_log)
    
    return 'Done!'
}

export function date(date: number){
    var usaTime = new Date(Number(date)).toLocaleString("en-US", {timeZone: "America/New_York"});
    return new Date(usaTime).toString().slice(4,-41)
}