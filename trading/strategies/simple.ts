import { complete_test_info } from '../../interfaces/index';
import { verifyStopLoss } from '../tools/stoploss';
import { loopBuyLayers, loopSellLayers } from '../layers/loop';

const debug = false

// Buy Strategy
export async function SimpleBuyStrategy(test: complete_test_info, trigger: boolean){
    // no trailing. every candle, il will loop layers
    // those layers will buy if trigger is true, if limit they will (continue) follow price
    test = await loopBuyLayers(test, trigger)

    return test
}

// Sell Strategy
export async function SimpleSellStrategy(test: complete_test_info, trigger: boolean){
    // no trailing. every candle, il will loop layers
    // those layers will sell if trigger is true, if limit they will (continue) follow price
    test = await verifyStopLoss(test) // stoploss will sell if price below x% latest bought price
    test = await loopSellLayers(test, trigger)

    return test
}