import * as config from '../config.json';
import fetch from 'node-fetch';
const debug = false
import { pool } from './index';
import { market, market_candle } from '../interfaces/index';
import { unlinkByMarket } from './test_trade';

export async function updateBaseMarkets(){
    let protocol = 'http'
    if (config.sputnik.https){
        protocol = 'https'
    }

    let exchanges = [] as any
    let jso = await fetch(`${protocol}://${config.sputnik.host}:${config.sputnik.port}/api/exchanges`)
    exchanges = await jso.json()
    let markets = exchanges[0].markets
    await removeLeftovers()
    await resetSputnikIDs()
    for(const market of markets){
        let existing = await getOneByName(market.name)
        if(typeof existing === 'undefined'){
            create(market)
        }else{
            market.sputnik_id = market.id
            market.id = existing.id
            update(market)
        }
    }
    await removeLeftovers()
}

export async function create(market: market) {
    let res = {} as any
    let text = 'INSERT INTO markets (name, sputnik_id) VALUES ($1, $2) '+
                'ON CONFLICT DO NOTHING RETURNING ID'
    let values = [market.name, market.id]
        try {
            res = await pool.query(text, values) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save market')
            }
        } catch (err) {
            console.log(err.stack, 'error market')
        }
    
    return res.rows[0] as market
}
export async function update(market: market) {
    let res = {} as any
    let text = 'UPDATE markets SET name = $1, sputnik_id = $2 WHERE id = $3;'
    let values = [
        market.name,
        market.sputnik_id > 0 ? market.sputnik_id : null,
        market.id
    ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update market')
    }
}

export async function unlink(id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM markets WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error delete market')
    }
}

async function resetSputnikIDs(){
    let allMarkets = await getAll()
    for(const market of allMarkets){
        market.sputnik_id = 0
        await update(market)
    }
}

async function removeLeftovers(){
    let allMarkets = await getAll()
    for(const market of allMarkets){
        if(market.sputnik_id < 1){
            //sputnik_id is null
            console.log('Removing market', market.name)
            await unlinkByMarket(market.id)
            await unlink(market.id)
        }
    }
}

export async function get(id = 0, sputnik = false) {
    let res = {} as any
    if(id === 0){
        try {
            res = await pool.query(`SELECT * FROM markets ORDER BY name ASC;`)
        } catch (err) {
            console.log(err.stack, 'error get market')
        }
    }else{
        if(sputnik){
            try {
                res = await pool.query('SELECT * FROM markets WHERE sputnik_id = $1 ORDER BY name ASC;', [id])
            } catch (err) {
                console.log(err.stack, 'error get market')
            }
            res.rows = res.rows[0] as market
        }else{
            try {
                res = await pool.query('SELECT * FROM markets WHERE id = $1 ORDER BY name ASC;', [id])
            } catch (err) {
                console.log(err.stack, 'error get market')
            }
            res.rows = res.rows[0] as market
        }
    }
    return res.rows as market[]
}

export async function getOne(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM markets WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error getOn market')
    }
    return res.rows[0] as market // return an array
}

export async function getSputnik(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM markets WHERE sputnik_id = ${id};', [id])
    } catch (err) {
        console.log(err.stack, 'error getSputnik market')
    }
    return res.rows[0] as market // return an array
}

export async function getOneByName(name: string) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM markets WHERE name = $1;', [name])
    } catch (err) {
        console.log(err.stack, 'error getOneByName market')
    }
    return res.rows[0] as market// return an array
}

export async function getAll() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM markets ORDER BY name ASC;`)
    } catch (err) {
        console.log(err.stack, 'error getAll market')
    }
    return res.rows as market[] // return an array
}


// connecting to sputnik API

export async function getExchanges() {
    let exchanges = [] as any
    try {
        let data = {} as any
        data = await pool.query(`SELECT distinct exchange from markets`)
        for (const ex of data.rows) {
            exchanges.push(ex)
        }
    } catch (err) {
        console.log(err.stack)
    }
    for (const [i, ex] of exchanges.entries()) {
        exchanges[i].markets = []
        try {
            let data = {} as any
            data = await pool.query('SELECT id, name FROM markets WHERE exchange = $1 ORDER BY id', [ex.exchange])
            for (const mark of data.rows) {
                exchanges[i].markets.push(mark)
            }
        } catch (err) {
            console.log(err.stsck)
        }
    }
    return exchanges
}


export async function getMarketList() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM markets ORDER BY id`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows as market[]
}


export async function getMarketInTimeframe(market_id: number, timeframe: number) {
    let protocol = 'http'
    if (config.sputnik.https){
        protocol = 'https'
    }
    let market_data = [] as any
    let jso = await fetch(`${protocol}://${config.sputnik.host}:${config.sputnik.port}/api/markets/${market_id}/${timeframe}`)
    market_data = await jso.json()
    //market_data = JSON.parse(market_data)
    return market_data as market_candle[]
}

let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export async function getMarketInTimeframePeriod(market_id: number, timeframe: number, start: number, stop: number) {
    let protocol = 'http'
    if (config.sputnik.https){
        protocol = 'https'
    }
    let market_data = [] as any
    let jso = {} as any

    let result = false
    let times = 1
    while(!result){
        try {
            jso = await fetch(`${protocol}://${config.sputnik.host}:${config.sputnik.port}/api/markets_by_period/${market_id}/${timeframe}/${start}/${stop}`)
            result = true
        } catch (error) {
            console.log(error, 'error retrieving sputnik data')
            result = false
            await sleep(times*1000)
            times++
        }
    }
    
    try{
        market_data = await jso.json()
    }catch(err){
        console.log(err, 'error converting to json', jso)
    }
    //market_data = JSON.parse(market_data)
    return market_data as market_candle[]
}

export async function getMarketInTimeframeSince(market_id: number, timeframe: number, start: number) {
    let protocol = 'http'
    if (config.sputnik.https){
        protocol = 'https'
    }
    let market_data = [] as any
    let jso = {} as any

    let result = false
    let times = 1
    while(!result){
        try {
            jso = await fetch(`${protocol}://${config.sputnik.host}:${config.sputnik.port}/api/markets_by_period_since/${market_id}/${timeframe}/${start}`)
        } catch (error) {
            console.log(error, 'error retrieving sputnik data')
            result = false
            await sleep(times*1000)
            times++
        }
    }
    try{
        market_data = await jso.json()
    }catch(err){
        console.log(err, 'error converting to json', jso)
    }
    //market_data = JSON.parse(market_data)
    return market_data as market_candle[]
}

export async function getMarketInTimeframeLast(market_id: number, timeframe: number, count: number) {
    let protocol = 'http'
    if (config.sputnik.https){
        protocol = 'https'
    }
    let market_data = [] as any
    let jso = {} as any
    let result = false
    let times = 1
    while(!result){
        try {
            jso = await fetch(`${protocol}://${config.sputnik.host}:${config.sputnik.port}/api/markets_last/${market_id}/${timeframe}/${count}`)
            result = true
        } catch (error) {
            console.log(error, 'error retrieving sputnik data')
            result = false
            await sleep(times*1000)
            times++
        }
    }
    try{
        market_data = await jso.json()
    }catch(err){
        console.log(err, 'error converting to json', jso)
    }
    //market_data = JSON.parse(market_data)
    return market_data as market_candle[]
}

export async function getLastByName(name: string, base: string){

    let market = await getOneByName(name+'/'+base)
    let latest = {} as market_candle
    if(typeof market !== 'undefined'){
        latest = (await getMarketInTimeframeLast(market.sputnik_id, 60000, 1))[0]
    }
    return latest
}
