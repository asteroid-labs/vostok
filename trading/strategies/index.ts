import { complete_test_info } from '../../interfaces/index';

/* strategies */
import { TrailBuyStrategy, TrailSellStrategy } from './trailing';
import { SimpleBuyStrategy, SimpleSellStrategy } from './simple';

/* tools */
import { write } from '../tools/logs';
import { testTriggers } from '../tools/indicators';
import { checkBalance } from '../tools/calculations'

let debug = false

export async function tradeLoop(test: complete_test_info){
    if(typeof test.markets[test.index].market_data[test.candle.i] !== 'undefined'){
        let buy_balance = await checkBalance(test, 'buy')
        if(buy_balance.result){
            let buy_trigger = await testTriggers(test, 'buy')
            if(buy_trigger){
                // all triggers are true here, will buy
                write(test.markets[test.index].market_data[test.candle.i][0],
                    test.markets[test.index].market.name,
                    'info',
                    test.original.id,
                    debug,
                    `: all buy triggers are true for price ${test.markets[test.index].market_data[test.candle.i][1].toFixed(7)} and account has funds, initiate buy layers`
                )
            } 
            if (test.algo.buy_pref.trail_trig > 0) {
                test = await TrailBuyStrategy(test, buy_trigger)
            } else {
                test = await SimpleBuyStrategy(test, buy_trigger)
            } 
        }
        buy_balance.result = false

        let sell_balance = await checkBalance(test, 'sell')
        if(sell_balance.result){
            let sell_trigger = await testTriggers(test, 'sell')
            if(sell_trigger){
                // all triggers are true here, will sell
                write(test.markets[test.index].market_data[test.candle.i][0],
                    test.markets[test.index].market.name,
                    'info',
                    test.original.id,
                    debug,
                    `: all sell triggers are true for price ${test.markets[test.index].market_data[test.candle.i][1].toFixed(7)} and account has funds, initiate sell layers`
                )
            } 
            if (test.algo.sell_pref.trail_trig > 0) {
                test = await TrailSellStrategy(test, sell_trigger)
            } else {
                test = await SimpleSellStrategy(test, sell_trigger)
            }
        }
        sell_balance.result = false
    }
    return test
}
