import express from 'express';
import * as db_user from '../db/user';
import child from 'child_process';
import * as config from '../config.json';
import { user } from '../interfaces/index';

let router: express.Router = express.Router();

router.post('',
    async function(req, res) {
        let auth = req.isAuthenticated()
        if (auth) {
                // save user's config
                let pref = {
                    telegram_id: req.body.telegram_id
                }
                let user = {
                    id: req.body.id,
                    name: req.body.name,
                    prefs: JSON.stringify(pref)
                } as user
                await db_user.update(user)
                if(req.body.telegram_id != ''){
                    // Start user notifs
                    try {
                        let notif = child.fork('./build/notifications/users.js')
                        notif.send({m: 'queryAccounts', queryAccounts: user.id})
                    } catch (err) {
                        console.log(err.stack, 'forking users notifs')
                    }
                }
                res.redirect('/user')
        } else {
            // not logged in
            res.redirect('/login');
        }
    }
)
router.get('', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        let admin = false
        let user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        user = await db_user.getUser(user.id)
        if(user.prefs){
            user.telegram_id = JSON.parse(JSON.stringify(user.prefs)).telegram_id
        }else{
            user.telegram_id = ''
        }
        res.render('pages/user.ejs', {auth, admin, user })
    } else {
        res.redirect('/login');
    }
})

export = router
