import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.ultosc);
    const high: number[] = data.map(candle => candle[2]);
    const low: number[] = data.map(candle => candle[3]);
    const close: number[] = data.map(candle => candle[4]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.ultosc.indicator([high, low, close], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate ultosc indicator: ${err}`);
    }
}