import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.obv);
    const close: number[] = data.map(candle => candle[4]);
    const volume: number[] = data.map(candle => candle[5]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.obv.indicator([close, volume], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate obv indicator: ${err}`);
    }
}