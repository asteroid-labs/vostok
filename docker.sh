#!/bin/bash
#docker run -d --name timescaledb-vostok -p 5434:5432 -e POSTGRES_USER=vostok -e POSTGRES_DB=vostok -e POSTGRES_PASSWORD=S3cret --restart=unless-stopped timescale/timescaledb:latest-pg11
echo "**** HAVE YOU FORGOT TO COPY THE CONFIG AND CERTS ?? ****"
docker stop vostok
docker rm vostok
docker build -t vostok .
docker run -d --name vostok -p 4000:4000 --network="host" --restart=unless-stopped -v /build/backtest/results vostok
docker start vostok
