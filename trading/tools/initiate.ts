import * as db_algo from '../../db/algo';
import * as db_market from '../../db/market';
import * as db_test from '../../db/tests';
import * as db_test_trade from '../../db/test_trade';
import * as db_test_logs from '../../db/test_logs';
import * as db_balance from '../../db/test_balance';
import { candle_info, complete_test_info, test_balance, market_test, market_candle, market } from '../../interfaces/index';

/* tools */
import { write } from './logs';
import { getCandles, getLastCandles } from '../tools/timeframes';
import { sendTestEnded } from '../../notifications';

let debug = false

export async function initiateTest(test_id: number, type: string, strategy: Function) {

    let test = await db_test.get(test_id)
    let flag = false // fast exit
    if(test.state === 'done'){ 
        write(
            Date.now(), 
            '',
            'info',
            test.id, 
            debug, 
            `${type} # ${test.id}: ${type} already ran before, exiting`
        )
        flag = true // return 0
    }
    // backtest is initiated, getting related info
    let algo = await db_algo.getOne(test.user_id, test.account_id, test.algo_id, true)
    if(algo.status === 'off'){ 
        write(
            Date.now(), 
            '',
            'info',
            test.id, 
            debug, 
            `${type} # ${test.id}: algo is in off status, exiting`
        )
        flag = true // return 0
    }else if((type === 'backtest' && algo.status !== 'backtesting') || (type === 'livetest' && algo.status !== 'livetesting') && test.state === 'init'){ 
        write(
            Date.now(), 
            '',
            'info',
            test.id, 
            debug, 
            `${type} # ${test.id}: warning, algo is in ${algo.status} status, it should be ${type}ing`
        )
    }else if(type === 'real' && algo.status !== 'realtrading'){ 
        write(
            Date.now(), 
            '',
            'info',
            test.id, 
            debug, 
            `${type} # ${test.id}: warning, algo is in ${algo.status} status, it should be should be Real Trading`
        )
        flag = true
    }

    let markets = []
    if(typeof test.pair_ids === 'object'){
        for(const pair of test.pair_ids){
            let market_test = {market: await db_market.getOne(Number(pair))} as market_test
            if(typeof market_test.market !== 'undefined'){
                markets.push(market_test)
            }
        }
    }else{
        let market_test = {market: await db_market.getOne(Number(test.pair_ids))} as market_test
        if(typeof market_test.market !== 'undefined'){
            markets.push(market_test)
        }
    }

    let test_info = {
        original: test,
        index: 0,
        algo: algo,
        type: type,
        markets: markets
    } as complete_test_info

    for(let market of markets){  
        // set quotes balances to 0
        // set btc balance to base balance
        if(type !== 'real' && test.state === 'init'){
            let quote_balance = {
                test_id: test.id,
                symbol: market.market.name.split("/")[0],
                free: 0,
                locked: 0
            } as test_balance
            //console.log('quote_balance', quote_balance)
            await db_balance.upsert(quote_balance)

            if(market.market.name.split("/")[1] === 'BTC'){
                let base_balance = {
                    test_id: test.id,
                    symbol: market.market.name.split("/")[1],
                    free: test.base_amount, //set btc amount
                    locked: 0
                } as test_balance
                await db_balance.upsert(base_balance)
            }else{
                // base is not btc, get price and set amount
                let market_name = 'BTC/' + market.market.name.split("/")[1]
                let data = {} as market_candle[]
                let market_data = {} as market
                try{
                    market_data = await db_market.getOneByName(market_name)
                }catch(err){
                    console.log(err, 'fetching diff balance id', market_name, data)
                }

                try{
                    data = await db_market.getMarketInTimeframeLast(market_data.sputnik_id, 60000, 1)
                }catch(err){
                    console.log(err, 'fetching diff balance price', market_name, data)
                }

                let diff_base_balance = {
                    test_id: test.id,
                    symbol: market.market.name.split("/")[1],
                    free: test.base_amount * data[0].opening_price,
                    locked: 0
                } as test_balance
                //console.log('diff_base_balance', diff_base_balance)
                await db_balance.upsert(diff_base_balance)
            }
        }
        if(flag){
            await endTest(test_info)
            return 0
        } else {
            try{
                let init = await initiateStrategy(test_info, market)
                if(typeof init != 'string'){
                   market = init
                }
            }catch(err){
                console.log(err, 'error')
                return 'error'
            }
        }
    }
    await strategy(test_info)
    return 0
}

export async function initiateStrategy(test: complete_test_info, market: market_test){
    
    /* need to check min daily volume */
    let daily_volume = await getCandles(market.market.sputnik_id, 86400000, Number(Number(test.original.start)-(2*86400000)), Number(Number(test.original.start)+86400000))
    
    if(daily_volume.length > 0 && daily_volume[0][5] <= Number(test.algo.markets_min_volume)){
        // daily volume for this market is too low, exiting
        if(test.type === 'backtest'){
            write(
                Date.now(), 
                market.market.name,
                'info',
                test.original.id, 
                debug, 
                `${test.type} : warning, ${market.market.name} daily volume is ${daily_volume[0][5]}, algo minimum should be higher than ${(test.algo.markets_min_volume)}`
            )
        }else{
            write(
                Date.now(), 
                market.market.name,
                'info',
                test.original.id, 
                debug, 
                `${test.type} : warning, ${market.market.name} daily volume is ${daily_volume[0][5]}, algo minimum should be higher than ${(test.algo.markets_min_volume)}, exiting`
            )
            await endTest(test)
            return 'Done!'
        }
    }else if(daily_volume.length === 0 && Number(test.algo.markets_min_volume) > 0){
        write(
            Date.now(), 
            market.market.name,
            'info',
            test.original.id, 
            debug, 
            `${test.type} : warning, cannot get ${market.market.name} daily volume (check sputnik instance), algo minimum should be higher than ${(test.algo.markets_min_volume)}, exiting`
        )
        await endTest(test)
        return 'Done!'
    }

    // for backtesting, get 1min market data in order to loop it
    // livetest will fetch new data every 3 seconds
    if(test.type === 'backtest') {
        market.market_data = await getCandles(market.market.sputnik_id, 60000, test.original.start, test.original.stop)
        if(market.market_data.length < 2){
            write(
                Date.now(), 
                market.market.name,
                'info',
                test.original.id, 
                debug, 
                `${test.type} : warning, cannot get ${market.market.name} data (check sputnik instance), exiting`
            )
            await endTest(test)
            return 'Done!'
        }
    }else{
        market.market_data = await getLastCandles(market.market.sputnik_id, 60000, 100)
    }

    // define candle, triggers, i
    if(test.type === 'backtest'){
        test.candle = {
            candle: market.market_data[0],
            i: 0,
        } as candle_info
    }else{
        test.candle = {
            candle: [0,0,0,0,0],
            i: 0,
        } as candle_info
    }
    
    // set trade info settings
    market.trade_info = {
        id: 0,
        last: {
            price: 0, 
            qty: 0
        },
        price: 0,
        base_amount_override: test.original.base_amount,
        trail_price: 0
    }
    //get latest trade
    let trade = await db_test_trade.getLast(test.original.id)

    market.last_trade_time = Number(test.original.start-1)

    if(typeof trade !== 'undefined'){
        // last trade exists, setting informations
        // market.trade_info.state = trade.side === 'buy' ? 'sell' : 'buy'
        //test.trade_info.last.qty = Number(trade.qty)
        // setting time/candle
        market.last_trade_time = Number(trade.time)
        await db_test_logs.unlinkNewer(test.original.id, Number(trade.time))
        /*         write(
            Date.now(), 
            market.market.name,
            'info',
            test.original.id, 
            debug, 
            `${test.type} : ${test.type} is restarted from ${new Date(Number(trade.time))}`
        ) */
    }else if(test.type === 'real' && test.original.state === 'init'){
        write(
            Date.now(), 
            market.market.name,
            'info',
            test.original.id, 
            debug, 
            `Starting Real Trading.`
        )
    }else if(test.original.state === 'init'){
        write(
            Date.now(), 
            market.market.name,
            'info',
            test.original.id, 
            debug, 
            `Starting ${test.type}.`
        )
    }

    // updates backtest status
    test.original = await db_test.update(test.original.id, 'running')

    return market
}

export async function endTest(test: complete_test_info){
    write(
        Date.now(), 
        '',
        'info',
        test.original.id, 
        debug, 
        `Ending ${test.type}.`
    )

    await db_test.update(test.original.id, 'done')
    await sendTestEnded(test.original.user_id, test.original.account_id, test.type, test.original.id)
}