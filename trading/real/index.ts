import { startStrategies } from '../livetest'
/* tools */
import { initiateTest } from '../tools/initiate';

process.on('message', async function(msg) {
    //console.log('Message from parent:', msg);
    if(msg.test_id){
        try{
            await initiateTest(msg.test_id, 'real', startStrategies)
        }catch(err){
            console.log(err.stack, 'error on real main()', msg.test_id)
        }
    }
    process.exit()
  });

