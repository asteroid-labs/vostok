import express from 'express';
import * as db_account from '../db/account';
import * as db_balance from '../db/real_balance';
import * as db_market from '../db/market';
import * as config from '../config.json';
import { accounts as random_accounts } from './random_names.json'
import child from 'child_process';
import { user, account } from '../interfaces/index';
import { shuffle } from './tools'

let router: express.Router = express.Router();

router.get('', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        let admin = false
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        const usr = req.user as user
        let accounts = await db_account.getAll(usr.id, true)
        for(const account of accounts){
            account.balances = await getBalances(account.id)
            if(account.prefs){
                let prefs = account.prefs as any
                account.email_notifications = prefs.email_notifications
                account.telegram_notifications = prefs.telegram_notifications
            }
        }
        res.render('pages/accounts.ejs', {auth, admin, accounts })
    } else {
        res.redirect('/login');
    }
})

router.post('/:account_id', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth) {
        const user = req.user as user
        const account_id = Number(req.params.account_id)
        let prev_account = await db_account.getOne(user.id, account_id)
        if (req.body.delete_account === '') {
            let del = await db_account.unlink(user.id, account_id)
            res.redirect(`/account/`);
        } else {
            // save account's config
            let pref = {
                email_notifications: req.body.email_notifications,
                telegram_notifications: req.body.telegram_notifications
            }
            let account = {
                id: account_id,
                user_id: user.id,
                name: req.body.name,
                exchange: req.body.exchange,
                api_key: req.body.api_key,
                api_secret: req.body.api_secret,
                trading_status: req.body.trading_status,
                prefs: JSON.stringify(pref)
            } as account
            await db_account.update(account)
            if(prev_account?.prefs){
                let prefs = account.prefs as any
                if(prefs.telegram_notifications === 'off' && req.body.telegram_notifications === 'on'){
                    // Start account notifs
                    try {
                        let notif = child.fork('./build/notifications/users.js')
                        notif.send({m: 'startAccountWS', startAccountWS: account, startAccountUser: user})
                    } catch (err) {
                        console.log(err.stack, 'forking account notifs')
                    }
                }
            }
            res.redirect('/account/' + account_id)
        }
    } else {
        // not logged in
        res.redirect('/login');
    }
})

router.get('/new', async function(req, res) {
    if (req.isAuthenticated() && req.user) {
        const user = req.user as user
        let acc = {
            id: 0,
            user_id: Number(user.id),
            name: random_accounts[await shuffle(random_accounts.length)],
            exchange: 1,
            api_key: 'Your Exchange API Key',
            api_secret: 'Your Exchange API Secret',
            trading_status: 'on',
            prefs: JSON.stringify({ email_notifications: 'on', telegram_notifications: 'off' })
        } as account
        acc.email_notifications = 'on'
        acc.telegram_notifications = 'off'
        let saving = await db_account.create(acc)
        let account = await db_account.getOne(Number(user.id), saving)
        res.redirect(`/account/${account.id}`);
    } else {
        res.redirect('/login');
    }
})

router.get('/:account_id', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
        let admin = false
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        let account = await db_account.getOne(user.id, Number(req.params.account_id))
        account.balances = await getBalances(account.id)
        if(account.prefs){
            let prefs = account.prefs as any
            account.email_notifications = prefs.email_notifications
            account.telegram_notifications = prefs.telegram_notifications
        }

        res.render('pages/accounts.ejs', {auth, admin, accounts: [account] })
    } else {
        res.redirect('/login');
    }
})

async function getBalances(id: number){
    let balances = await db_balance.getAll(id, true)
    for(const balance of balances){
        let latest_btc = await db_market.getLastByName(balance.symbol, 'BTC')
        balance.latest_btc_price = typeof latest_btc !== 'undefined' ? latest_btc.closing_price : 1
        if(balance.symbol === 'BTC'){
            balance.latest_btc_price = 1
        }
    }
    return balances
}

export = router
