import * as db from './db/index';
import * as db_account from './db/account';
import * as db_exchange from './db/exchange';
import * as db_timeframe from './db/timeframe';
import * as db_indicator from './db/indicator';
import * as db_market from './db/market';
import * as config from './config.json'
import child from 'child_process';
import { user } from './interfaces/index';
import { fetchLastOtherSideTrade } from './binance';

async function main() {

    let account = await db_account.getOne(1, 4)
    let test = await fetchLastOtherSideTrade(account, {id: 1, sputnik_id: 1, name:'WRX/BTC'})
}

main()