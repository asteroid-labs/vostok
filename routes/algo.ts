import express from 'express';
import * as db_algo from '../db/algo';
import * as db_account from '../db/account';
import * as db_timeframe from '../db/timeframe';
import * as db_indicator from '../db/indicator';
import * as db_tests from '../db/tests';
import * as db_test_trade from '../db/test_trade';
import { algo, user, algo_layers, algo_indicator, trail, test } from '../interfaces/index';
import * as config from '../config.json';
import { shuffle } from './tools'
import { algos as random_algos } from './random_names.json'

let router: express.Router = express.Router();

router.get('', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        let admin = false
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        let algos = await db_algo.getAllFromUser(user.id)
        for(const algo of algos){
            algo.tests = {backtest: [] as test[], livetest:[] as test[], realtest:[] as test[]}
            algo.tests.backtest = await db_tests.getRunningByAlgo(algo.id, 'backtest')
            algo.tests.livetest = await db_tests.getRunningByAlgo(algo.id, 'livetest')
            algo.tests.realtest = await db_tests.getRunningByAlgo(algo.id, 'real')

            let tests = await db_tests.getAllByAlgo(algo.id)
            let total_percents = [0]
            for(const test of tests){
                let trades = await db_test_trade.getChilds(test.id)
                test.total = 0
                test.total_percent = 0
                if(trades.length > 0){
                    let data = await db_test_trade.getTradeGains(test.id, trades[trades.length - 1].id as number)
                    test.total = data.gain
                    //test.total_percent = data.percent
                    total_percents.push(data.percent)
                }
            }
            total_percents = total_percents.filter((n: number) => n !== 0)
            if(total_percents.length > 0){
                algo.profits = total_percents.reduce((a,b) => (a+b)) / total_percents.length
            }
        }
        res.render('pages/algos.ejs', {auth, admin, algos })
    } else {
        res.redirect('/login');
    }
})

router.post('/:algo_id',async function(req, res) {
    if (req.isAuthenticated()) {
        const user = req.user as user
        let algo_id = Number(req.params.algo_id)

        if (req.body.delete_algo === '') {
            await db_algo.unlink(Number(user.id), Number(algo_id))
            res.redirect(`/algo/`);
        } else if(req.body.duplicate === ''){
            // get algo config from id
            let new_algo = await db_algo.getOneFromUser(user.id, algo_id)
            // change name
            new_algo.name = 'Duplicate of '+new_algo.name
            // create a new one
            let saved = await db_algo.create(new_algo)
            // redirect to it
            res.redirect('/algo/' + saved.id)
        } else {
            // save account's config
            //console.log(req.body)
            //req.body = req.body as posted_algo
            //let old = await db_algo.getOneFromUser(user.id, algo_id)
            let buy_indicators = [] as algo_indicator[]
            if(Array.isArray(req.body.buy_indicator_timeframe_id)){
                // multiple indicators
                req.body.buy_indicator_timeframe_id.forEach((timeframe: number, i: number) => {
                    if(req.body.buy_indicator_trigger[i] !== ''){
                        buy_indicators.push({
                            id: Number(req.body.buy_indicator_id[i]),
                            name: '',
                            trigger_cond: req.body.buy_indicator_trigger_cond[i],
                            trigger: Number(req.body.buy_indicator_trigger[i]),
                            timeframe:{
                                id: Number(timeframe),
                                msec_value: 0,
                            },
                            fnc: Function(),
                            periods: [0]
                        })
                    }
                })
            }else if(req.body.buy_indicator_trigger !== ''){
                // single indicator
                buy_indicators.push({
                    id: Number(req.body.buy_indicator_id),
                    name: '',
                    trigger_cond: req.body.buy_indicator_trigger_cond,
                    trigger: Number(req.body.buy_indicator_trigger),
                    timeframe:{
                        id: Number(req.body.buy_indicator_timeframe_id),
                        msec_value: 0,
                    },
                    fnc: Function(),
                    periods: [0]
                })
            }
            let buy_layers = [] as algo_layers[]
            if(Array.isArray(req.body.buy_layer_type)){
                // multiple indicators
                req.body.buy_layer_type.forEach((type: string, i: number) => {
                    if(req.body.buy_layer_qty[i] !== '' && req.body.buy_layer_qty[i] !== 0){
                        buy_layers.push({
                            type: type,
                            percent: Number(req.body.buy_layer_percent[i]),
                            qty: Number(req.body.buy_layer_qty[i]),
                            qty_percent: req.body.buy_layer_qty_percent[i],
                            stall: Number(req.body.buy_layer_stall[i]),
                            stall_protect: { active: false, current: 0},
                            price: 0
                        })
                    }
                })
            }else if(req.body.buy_layer_qty !== '' && req.body.buy_layer_qty !== 0){
                // single indicator
                buy_layers.push({
                    type: req.body.buy_layer_type,
                    percent: Number(req.body.buy_layer_percent),
                    qty: Number(req.body.buy_layer_qty),
                    qty_percent: req.body.buy_layer_qty_percent,
                    stall: Number(req.body.buy_layer_stall),
                    stall_protect: { active: false, current: 0},
                    price: 0
                })
            }
            let buy = {
                indicators: buy_indicators,
                layers: buy_layers,
                layer_index: 0,
                trail_trig: Number(req.body.buy_trail_trig),
                trail_gap_reduce: Number(req.body.buy_trail_gap_reduce),
                trail_stop: Number(req.body.buy_trail_stop),
                trail: {active: false, trigger: 0, stop: 0}
            }

            let sell_indicators = []
            if(Array.isArray(req.body.sell_indicator_timeframe_id)){
                // multiple indicators
                req.body.sell_indicator_timeframe_id.forEach((timeframe: number, i: number) => {
                    if(req.body.sell_indicator_trigger[i] !== ''){
                        sell_indicators.push({
                            id: Number(req.body.sell_indicator_id[i]),
                            trigger_cond: req.body.sell_indicator_trigger_cond[i],
                            trigger: Number(req.body.sell_indicator_trigger[i]),
                            timeframe:{
                                id: Number(timeframe),
                                msec_value: 0,
                                market_data: [[0]],
                            },
                            name: '',
                            fnc: Function(),
                            periods: [0]
                        })
                    }
                })
            }else if(req.body.sell_indicator_trigger !== ''){
                // single indicator
                sell_indicators.push({
                    id: Number(req.body.sell_indicator_id),
                    trigger_cond: req.body.sell_indicator_trigger_cond,
                    trigger: Number(req.body.sell_indicator_trigger),
                    timeframe:{
                        id: Number(req.body.sell_indicator_timeframe_id),
                        msec_value: 0,
                        market_data: [[0]],
                    },
                    name: '',
                    fnc: Function(),
                    periods: [0]
                })
            }

            let sell_layers = [] as algo_layers[]
            if(Array.isArray(req.body.sell_layer_type)){
                // multiple indicators
                req.body.sell_layer_type.forEach((type: string, i: number) => {
                    if(req.body.sell_layer_qty[i] !== '' && req.body.sell_layer_qty[i] !== 0){
                        sell_layers.push({
                            type: type,
                            percent: Number(req.body.sell_layer_percent[i]),
                            qty: Number(req.body.sell_layer_qty[i]),
                            qty_percent: req.body.sell_layer_qty_percent[i],
                            stall: Number(req.body.sell_layer_stall[i]),
                            stall_protect: { active: false, current: 0},
                            price: 0
                        })
                    }
                })
            }else if(req.body.sell_layer_qty !== '' && req.body.sell_layer_qty !== 0){{
                    // single indicator
                    sell_layers.push({
                        type: req.body.sell_layer_type,
                        percent: Number(req.body.sell_layer_percent),
                        qty: Number(req.body.sell_layer_qty),
                        qty_percent: req.body.sell_layer_qty_percent,
                        stall: Number(req.body.sell_layer_stall),
                        stall_protect: { active: false, current: 0},
                        price: 0
                    })
                }
            }

            let sell = {
                indicators: sell_indicators,
                layers: sell_layers,
                layer_index: 0,
                stop_loss: Number(req.body.sell_stop_loss),
                stop_loss_limit: Number(req.body.sell_stop_loss_limit),
                trail_trig: Number(req.body.sell_trail_trig),
                trail_gap_reduce: Number(req.body.sell_trail_gap_reduce),
                trail_stop: Number(req.body.sell_trail_stop),
                trail: {active: false, trigger: 0, stop: 0}
            }

            let algo = {
                id: algo_id,
                user_id: user.id,
                account_id: req.body.account_id,
                name: req.body.name,
                markets_min_volume: req.body.markets_min_volume,
                buy_pref: buy,
                sell_pref: sell,
                status: req.body.status
            }
            await db_algo.update(algo)
            res.redirect('/algo/' + algo_id)
        }
    } else {
        // not logged in
        res.redirect('/login');
    }
})

router.get('/new/:account_id', async function(req, res) {
    if (req.isAuthenticated() && req.user) {
        const user = req.user as user
            const account_id = Number(req.params.account_id)
                // create an algo linked to this user
                let buy = {
                        indicators: [{id: 0,
                        name: '',
                        trigger_cond: '<=', // '=', '>'
                        trigger: 0,
                        fnc: Function,
                        periods: [0],
                        timeframe: {id: 0,
                        msec_value: 0,
                        market_data: [[0]]
                    }}],
                    layers: [{type: 'market', qty: 0, qty_percent: false, price: 0, percent: 0, stall: 0, stall_protect:{active: false, current:0}}],
                    layer_index:0,
                    trail_trig: 0,
                    trail_gap_reduce: 0,
                    trail_stop: 0,
                    trail: {active: false, trigger: 0, stop: 0} as trail
                }
                let sell = {
                        indicators: [{id: 0,
                        name: '',
                        trigger_cond: '>=', // '=', '>'
                        trigger: 0,
                        fnc: Function,
                        periods: [0],
                        timeframe: {id: 0,
                        msec_value: 0,
                        market_data: [[0]]
                    }}],
                    layers: [{type: 'market', qty: 0, qty_percent: false, price: 0, percent: 0, stall: 0, stall_protect:{active: false, current:0}}],
                    layer_index:0,
                    stop_loss: 0,
                    stop_loss_limit: 0,
                    trail_trig: 0,
                    trail_gap_reduce: 0,
                    trail_stop: 0,
                    trail: {active: false, trigger: 0, stop: 0} as trail
                }
                let algo = {
                    id: 0,
                    user_id: user.id,
                    account_id: account_id,
                    name: random_algos[await shuffle(random_algos.length)],
                    status: "off",
                    markets_min_volume: 0,
                    buy_pref: buy,
                    sell_pref: sell,

                } as algo
                let created = await db_algo.create(algo)
                algo.accounts = await db_account.getAll(user.id)
                algo.timeframes = await db_timeframe.getAll()
                algo.buy = JSON.stringify(algo.buy_pref)
                algo.sell = JSON.stringify(algo.sell_pref)
                algo.indicators = await db_indicator.getAll()
                algo.id = created.id
                res.redirect('/algo/'+algo.id)
    } else {
        res.redirect('/login');
    }
})

router.get('/:algo_id', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        let admin = false
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        let algo_id = Number(req.params.algo_id)
        let algo = await db_algo.getOneFromUser(user.id, algo_id)
        algo.buy = JSON.stringify(algo.buy_pref)
        algo.sell = JSON.stringify(algo.sell_pref)

        algo.accounts = await db_account.getAll(user.id)
        algo.timeframes = await db_timeframe.getAll()
        algo.indicators = await db_indicator.getAll()

        algo.tests = {backtest: [] as test[], livetest:[] as test[], realtest:[] as test[]}
        algo.tests.backtest = await db_tests.getRunningByAlgo(algo.id, 'backtest')
        algo.tests.livetest = await db_tests.getRunningByAlgo(algo.id, 'livetest')
        algo.tests.realtest = await db_tests.getRunningByAlgo(algo.id, 'realtest')

        let tests = await db_tests.getAllByAlgo(algo.id)
        let total_percents = [0]
        for(const test of tests){
            let trades = await db_test_trade.getChilds(test.id)
            test.total = 0
            test.total_percent = 0
            if(trades.length > 0){
                let data = await db_test_trade.getTradeGains(test.id, trades[trades.length - 1].id as number)
                test.total = data.gain
                //test.total_percent = data.percent
                total_percents.push(data.percent)
            }
        }
        total_percents = total_percents.filter((n: number) => n !== 0)
        if(total_percents.length > 0){
            algo.profits = total_percents.reduce((a,b) => (a+b)) / total_percents.length
        }
        let algos = [algo]
        res.render('pages/algos.ejs', {auth, admin, algos })
    } else {
        res.redirect('/login');
    }
})

export = router
