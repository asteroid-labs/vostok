import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.aaroon;
    const high: number[] = data.map(candle => candle[2]);
    const low: number[] = data.map(candle => candle[3]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.aroon.indicator([high, low], period);
        return results[1]
    } catch (err) {
        throw new Error(`Failed to evaluate aroon_up indicator: ${err}`);
    }
}