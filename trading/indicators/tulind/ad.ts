import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.ad);
    const high: number[] = data.map(candle => candle[2]);
    const low: number[] = data.map(candle => candle[3]);
    const close: number[] = data.map(candle => candle[4]); // [4,5,6,6,6,5,5,5,6,4]
    const volume: number[] = data.map(candle => candle[5]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.ad.indicator([high, low, close, volume], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate AD indicator: ${err}`);
    }
}