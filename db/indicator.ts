const debug = false
import { pool } from './index';
import { indicator } from '../interfaces/index';

export async function createBaseIndicators() {

    let data = [
        {'name': 'AD - Accumulation/Distribution Line', 'function': 'ad', 'periods': []},
        //{'name': 'Adosc - Accumulation/Distribution Oscillator', 'function': 'adosc', 'periods': [2, 5]},
        {'name': 'Adx - Average Directional Movement Index', 'function': 'adx', 'periods': [14]},
        {'name': 'AdxR - Average Directional Movement Rating', 'function': 'adxr', 'periods': [14]},
        {'name': 'Ao - Awesome Oscillator', 'function': 'ao', 'periods': [14]},
        //{'name': 'Apo - Absolute Price Oscillator', 'function': 'apo', 'periods': [2, 5]},
        {'name': 'AroonU - Aroon Up Signal', 'function': 'aroon_up', 'periods': [14]},
        {'name': 'AroonD - Aroon Down Signal', 'function': 'aroon_down', 'periods': [14]},
        {'name': 'AroonOsc - Aroon Oscillator', 'function': 'aroonosc', 'periods': [14]},
        {'name': 'Atr - Average True Range', 'function': 'atr', 'periods': [14]},
        //{'name': 'AvgPrice - Average Price', 'function': 'avgprice', 'periods': []},
        {'name': 'BBandL - Bollinger Bands Lower', 'function': 'bbands', 'periods': [20, 2]},
        {'name': 'BBandM - Bollinger Bands Middle', 'function': 'bbands', 'periods': [20, 2]},
        {'name': 'BBandU - Bollinger Bands Upper', 'function': 'bbands', 'periods': [20, 2]},
        {'name': 'Bop - Balance of Power', 'function': 'bop', 'periods': []},
        {'name': 'Cci - Commodity Channel Index', 'function': 'cci', 'periods': [20]},
        {'name': 'Cmo - Chande Momentum Oscillator', 'function': 'cmo', 'periods': [9]},
        //{'name': 'Cvi - Chaikins Volatility', 'function': 'cvi', 'periods': [5]},
        {'name': 'Dema - Double Exponential Moving Average', 'function': 'dema', 'periods': [9]},
        //{'name': 'Di+ - Directional Indicator Plus Signal', 'function': 'di_plus', 'periods': [5]},
        //{'name': 'Di- - Directional Indicator Minus Signal', 'function': 'di_minus', 'periods': [5]},
        {'name': 'Dm+ - Directional Movement Plus Signal', 'function': 'dm_plus', 'periods': [14]},
        {'name': 'Dm- - Directional Movement Minus Signal', 'function': 'dm_minus', 'periods': [14]},
        {'name': 'Dpo - Detrended Price Oscillator', 'function': 'dpo', 'periods': [21]},
        //{'name': 'Dx - Directional Movement Index', 'function': 'dx', 'periods': [5]},
        {'name': 'Ema - Exponential Moving Average', 'function': 'ema', 'periods': [9]},
        {'name': 'Emv - Ease of Movement', 'function': 'emv', 'periods': [14]},
        {'name': 'Fisher - Fisher Transform', 'function': 'fisher', 'periods': [9]},
        {'name': 'FisherS - Fisher Transform Signal', 'function': 'fisher_signal', 'periods': [9]},
        //{'name': 'Fosc - Forecast Oscillator', 'function': 'fosc', 'periods': [5]},
        {'name': 'Hma - Hull Moving Average', 'function': 'hma', 'periods': [9]},
        //{'name': 'Kama - Kaufman Adaptive Moving Average', 'function': 'kama', 'periods': [5]},
        //{'name': 'Kvo - Klinger Volume Oscillator', 'function': 'kvo', 'periods': [2, 5]},
        {'name': 'LinReg - Linear Regression', 'function': 'linreg', 'periods': [100]},
        {'name': 'LinReg - Linear Regression Intercept', 'function': 'linregintercept', 'periods': [100]},
        {'name': 'LineRegSlope - Linear Regression Slope', 'function': 'linregslope', 'periods': [100]},
        {'name': 'Macd - Moving Average Convergence/Divergence', 'function': 'macd', 'periods': [12, 26, 9]},
        {'name': 'MacdS - Moving Average Convergence/Divergence Signal', 'function': 'macd_signal', 'periods': [12, 26, 9]},
        {'name': 'MacdH - Moving Average Convergence/Divergence Histogram', 'function': 'macd_histogram', 'periods': [12, 26, 9]},
        //{'name': 'MarketFi - Market Facilitation Index', 'function': 'marketfi', 'periods': []},
        {'name': 'Mass - Mass Index', 'function': 'mass', 'periods': [10]},
        {'name': 'MedPrice - Median Price', 'function': 'medprice', 'periods': [14]},
        {'name': 'Mfi - Money Flow Index', 'function': 'mfi', 'periods': [14]},
        {'name': 'Mom - Momentum', 'function': 'mom', 'periods': [10]},
        //{'name': 'Msw - Mesa Sine Wave', 'function': 'msw', 'periods': [5]},
        //{'name': 'Natr - Normalized Average True Range', 'function': 'natr', 'periods': [5]},
        //{'name': 'Nvi - Negative Volume Index', 'function': 'nvi', 'periods': [5]},
        {'name': 'Obv - On Balance Volume', 'function': 'obv', 'periods': []},
        {'name': 'Ppo - Percentage Price Oscillator', 'function': 'ppo', 'periods': [10, 21]},
        {'name': 'Psar - Parabolic SAR', 'function': 'psar', 'periods': [0.2, 2]},
        //{'name': 'Pvi - Positive Volume Index', 'function': 'pvi', 'periods': []},
        //{'name': 'Qstick - Qstick', 'function': 'qstick', 'periods': [5]},
        {'name': 'Roc - Rate of Change', 'function': 'roc', 'periods': [9]},
        {'name': 'Rocr - Rate of Change Ratio', 'function': 'rocr', 'periods': [9]},
        {'name': 'Rsi - Relative Strength Index', 'function': 'rsi', 'periods': [14]},
        {'name': 'Sma - Simple Moving Average', 'function': 'sma', 'periods': [9]},
        {'name': 'StochK - 	Stochastic Oscillator K Signal', 'function': 'stoch_k', 'periods': [14, 1, 3]},
        {'name': 'StochD - 	Stochastic Oscillator D Signal', 'function': 'stoch_d', 'periods': [14, 1, 3]},
        {'name': 'StochRsi - Stochastic Rsi', 'function': 'stochrsi', 'periods': [14]},
        {'name': 'Tema - Triple Exponential Moving Average', 'function': 'tema', 'periods': [9]},
        //{'name': 'Tr - True Range', 'function': 'tr', 'periods': []},
        //{'name': 'Trima - Triangular Moving Average', 'function': 'trima', 'periods': [5]},
        {'name': 'Trix - 	Trix', 'function': 'trix', 'periods': [18]},
        //{'name': 'Tsf - Time Series Forecast', 'function': 'tsf', 'periods': [5]},
        //{'name': 'TypPrice - Typical Price', 'function': 'typprice', 'periods': []},
        {'name': 'UltOsc - 	Ultimate Oscillator', 'function': 'ultosc', 'periods': [7, 14, 28]},
        //{'name': 'Vhf - Vertical Horizontal Filter', 'function': 'vhf', 'periods': [5]},
        //{'name': 'Vidya - Variable Index Dynamic Average', 'function': 'vidya', 'periods': [2, 5, 0.2]},
        {'name': 'Volatility - Annualized Historical Volatility', 'function': 'volatility', 'periods': [10]},
        {'name': 'Vosc - Volume Oscillator', 'function': 'vosc', 'periods': [5, 10]},
        //{'name': 'Vwma - Volume Weighted Moving Average', 'function': 'vwma', 'periods': [5]},
        //{'name': 'Wad - Williams Accumulation/Distribution', 'function': 'wad', 'periods': []},
        //{'name': 'Wcprice - Weighted Close Price', 'function': 'wcprice', 'periods': []},
        //{'name': 'Wilders - Wilders Smoothing', 'function': 'wilders', 'periods': [5]},
        {'name': 'WillR - Williams %R', 'function': 'willr', 'periods': [14]},
        {'name': 'Wma - Weighted Moving Average', 'function': 'wma', 'periods': [9]},
        //{'name': 'Zlema - Zero-Lag Exponential Moving Average', 'function': 'zlema', 'periods': [5]},
    ] as indicator[]
    let existing = await getAll()
    if(existing.length === 0){
        for (const item of data){
            create(item)
        }
    }
}

export async function create(indicator: indicator) {
    let res = {} as any
    let text = 'INSERT INTO indicators (name, function, periods) VALUES ($1, $2, $3) '+
                'ON CONFLICT DO NOTHING RETURNING ID;'
    let values = [
        indicator.name,
        indicator.function,
        JSON.stringify(indicator.periods)
    ]
    try {
        res = await pool.query(text, values) // upsert
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done save indicator')
        }
    } catch (err) {
        console.log(err.stack, 'error indicator')
    }
    return res.rows[0] as indicator
}

export async function update(indicator: indicator) {
    let res = {} as any
    let text = 'UPDATE indicators SET name = $1, function = $2, periods = $3 WHERE id = $4;'
    let values = [
        indicator.name,
        indicator.function,
        JSON.stringify(indicator.periods),
        indicator.id
    ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update indicator')
    }
    return true
}

export async function unlink(id: number) {
    let res = {} as any
    let text = 'DELETE FROM indicators WHERE id = $1'
    try {
        res = await pool.query(text, [id])
    } catch (err) {
        console.log(err.stack, 'error delete indicator')
    }
    return true
}

export async function getOne(id: number) {
    let res = {} as any

    try {
        res = await pool.query('SELECT * FROM indicators WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error get indicator')
    }
    return res.rows[0] as indicator
}

export async function getAll() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM indicators ORDER BY name ASC;`)
    } catch (err) {
        console.log(err.stack, 'error get indicator all')
    }
    return res.rows as indicator[]
}

