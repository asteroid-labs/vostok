import * as db_exchange from './exchange';
import * as db_algo from './algo';
import { pool } from './index';
import { account } from '../interfaces/index';
const debug = false

export async function create(account: account) {
    let res = {} as any
    let text = 'INSERT INTO accounts (user_id, name, exchange, api_key, api_secret, trading_status, prefs) '+
                'VALUES ($1, $2, $3, $4, $5,  $6, $7) ON CONFLICT DO NOTHING RETURNING id;'
    let values = [account.user_id,
                account.name,
                account.exchange,
                account.api_key,
                account.api_secret,
                account.trading_status,
                account.prefs
    ]
    if (account.user_id) {
        try {
            res = await pool.query(text, values) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save account')
            }
        } catch (err) {
            console.log(err.stack, 'error account')
        }
    }
    return res.rows[0].id as number
}

export async function update(account: account) {
    let res = {} as any
    let text = 'UPDATE accounts SET user_id = $1, name = $2, exchange = $3, api_key= $4, '+
                'api_secret = $5, prefs = $6 WHERE id = $7;'
    let values = [
        account.user_id,
        account.name,
        account.exchange,
        account.api_key,
        account.api_secret,
        account.prefs,
        account.id
    ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update account')
    }
}

export async function unlink(user_id: number, id: number) {
    let res = {} as any
    let text = "UPDATE accounts SET trading_status = 'deleted'"+
                'WHERE user_id = $1 AND id = $2;'
    let values =[user_id, id]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error delete account')
    }
}

export async function getOne(user_id: number, account_id: number) {
    let res = {} as any
    let text = 'SELECT * FROM accounts WHERE id = $1 AND user_id = $2;'
    let values = [account_id, user_id]
    let exchanges = await db_exchange.getAll()
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error getOne account')
    }
    res.rows[0].exchanges = exchanges
    res.rows[0].algos = await db_algo.getAllFromAccount(user_id, account_id)
    return res.rows[0] as account
}

export async function getAll(user_id: number, generateAlgo = true) {
    let res = {} as any
    let text = "SELECT * FROM accounts WHERE user_id = $1 AND trading_status != 'deleted' ORDER BY id ASC"
    let values = [user_id]
    
    let exchanges = await db_exchange.getAll()
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error getAll account')
    }
    for (const row of res.rows) {
        row.exchanges = exchanges
        generateAlgo ? row.algos = await db_algo.getAllFromAccount(user_id, row.id as number) : false
    }
    return res.rows as account[]
}