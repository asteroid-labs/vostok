import { account, market, binance_order } from '../interfaces/index';
import * as db_balance from '../db/real_balance';
const ccxt = require ('ccxt')

export async function fetchMarkets(){
    // create new exhcange class
    let binance = new ccxt.binance({
        'timeout': 30000,
        'enableRateLimit': true,
    })

    let markets = []
    try {
        markets = await binance.loadMarkets()
    }catch(err){
        console.log(err, 'error fetch markets')
    }

    return markets
}

export async function fetchLastOtherSideTrade(account: account, symbol: market){

    // create new exhcange class
    let binance = new ccxt.binance({
        'apiKey': account.api_key,
        'secret': account.api_secret,
        'timeout': 30000,
        'enableRateLimit': true,
    })

    //query the last order on this market in opposite side
    //let trades = []
    let orders = []
    try{
        orders = await binance.fetchClosedOrders(symbol.name)
        //trades = await binance.fetchMyTrades(symbol.name)
    }catch(err){
        console.log(err, 'error fetch trades', account, symbol)
    }

    orders = await orders.filter((o: any) => o.side === 'buy')

    //console.log('mapped orders', orders)
    let prev_order = {} as any
    if(typeof orders[orders.length - 1] !== 'undefined'){
        prev_order = orders[orders.length - 1]
        //console.log('prev order', prev_order)
    }else{
        prev_order = orders[0]
    }

    return prev_order as binance_order
}

export async function cancelLastSymbolOrder(account: account, symbol: market, side: string){
    let binance = new ccxt.binance({
        'apiKey': account.api_key,
        'secret': account.api_secret,
        'timeout': 30000,
        'enableRateLimit': true,
    })

    //query the last order on this market on same side
    
    let orders = {} as any
    try{
        orders = await binance.fetchOpenOrders(symbol.name, null, 2)
    }catch(err){
        console.log(err, 'error fetch open orders')
    }
    orders = orders.map((o: any) => { 
        if(side === o.side) { return o } }
    )
    orders = orders.sort((a:any, b:any) => a.timestamp - b.timestamp)
    let prev_order = orders[orders.length - 1]
    if(typeof prev_order.id !== 'undefined'){
        try{
            await binance.cancelOrder(prev_order.id, symbol.name)
        }catch(err){
            console.log(err, 'error cancel order')
        }
    }
}

export async function updateAllBalances(account: account){
    let binance = new ccxt.binance({
        'apiKey': account.api_key,
        'secret': account.api_secret,
        'timeout': 30000,
        'enableRateLimit': true,
    })

    //verifiy available balance
    let balances = {info:{balances:[{asset:'', free:'', locked: ''}]}}
    try{
        balances = await binance.fetchBalance()
    }catch(err){
        console.log(err, 'error getting balance')
    }
    //console.log(balances.info.balances)
    for(const b of balances.info.balances){
        let balance = {
            account_id: account.id,
            symbol: b.asset,
            free: parseFloat(b.free),
            locked: parseFloat(b.locked)
        }
        await db_balance.upsert(balance)
    }
}

export async function postMarketOrder(account: account, symbol: market, side: string, qty: number, backoff=0){
    let binance = new ccxt.binance({
        'apiKey': account.api_key,
        'secret': account.api_secret,
        'timeout': 30000,
        'enableRateLimit': true,
    })
    try {
        let result = await binance.createOrder(symbol.name, 'market', side, (qty * ( 1 - ( backoff / 100 ) ) ))
    }catch(err){
        console.log(err, 'error market binance order', symbol.name, 'limit', side, qty, backoff, (qty * ( 1 - ( backoff / 100 ) ) ))
        if(err.name === 'InsufficientFunds'){
            // retry with less funds
            console.log('retry with less funds')
            backoff++
            await postMarketOrder(account, symbol, side, qty, backoff)
        }
    }
}

export async function postLimitOrder(account: account, symbol: market, side: string, qty: number, price: number){
    let binance = new ccxt.binance({
        'apiKey': account.api_key,
        'secret': account.api_secret,
        'timeout': 30000,
        'enableRateLimit': true,
    })

    //verifiy available balance
    //let balances = await binance.fetchBalance()
    try{
        let result = await binance.createOrder(symbol.name, 'limit', side, qty, price)
    }catch(err){
        console.log(err, 'error limit binance order', symbol.name, 'limit', side, qty, price)
    }
}