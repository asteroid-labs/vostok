import * as db_algo from '../../algo'
import * as db_timeframe from '../../timeframe'
import * as db_indicator from '../../indicator'
import * as db_market from '../../market'
import { algo_pref, timeframe, indicator, algo } from 'interfaces/index';

export async function createTestAlgo(user_id: number, account_id: number){
  let timeframe = {
    "id" : 1,
    "name": 'Testing',
    "msec_value": 3600000
  } as timeframe 
  let nil_tf = await db_timeframe.create(timeframe)

  let indicator = {
    "id" : 1,
    "name": 'Testing RSI',
    "function": 'rsi',
    "periods": [14]
  } as indicator 
  let indi = await db_indicator.create(indicator)
  
  /* let market = {
    "id" : 1,
    "name": 'Testing ETH/BTC',
    "sputnik_id": 19
  } as market
  let nil_market = await db_market.create(market)
 */
  await db_market.updateBaseMarkets()
  let nil_market = await db_market.getOneByName('ETH/BTC')
  let second_market = await db_market.getOneByName('XLM/BTC')

  let algo = {
      "id": 5,
      "user_id": user_id,
      "account_id": account_id,
      "name": 'my algo5',
      "timeframe": nil_tf.id,
      "markets_min_volume": 0,
      "buy_pref": {} as algo_pref,
      "sell_pref": {} as algo_pref,
      "status": 'backtesting'
    } as algo
    let result = await db_algo.create(algo)
    console.log(result, algo)
    return {data: algo, user: user_id, account: account_id, algo:result.id, indicator: indi.id, timeframe: nil_tf.id, market: nil_market.id, markets: [nil_market.id, second_market.id]}
}

export async function updateMarketAlgo (user_id: number, account_id: number, algo_id: number, timeframe_id: number, indicator:number){

  let algo = {
    "id": algo_id,
    "user_id": user_id,
    "account_id": account_id,
    "name": 'my algo market test',
    "timeframe": timeframe_id,
    "markets_min_volume": 0,
    "buy_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":"<=","trigger":30,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}],"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "sell_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":">=","trigger":60,"timeframe":{"id":timeframe_id,"msec_value":0},"fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}], "stop_loss":0,"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "status": 'backtesting'
  } as algo
 
  await db_algo.update(algo)
  return {data: algo, algo:algo_id, algo_result: algo}
}

export async function updateMarketStopLossAlgo (user_id: number, account_id: number, algo_id: number, timeframe_id: number, indicator:number){

  let algo = {
    "id": algo_id,
    "user_id": user_id,
    "account_id": account_id,
    "name": 'my algo market test',
    "timeframe": timeframe_id,
    "markets_min_volume": 0,
    "buy_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":"<=","trigger":30,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}],"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "sell_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":">=","trigger":60,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}], "stop_loss":0.001, "stop_loss_limit":0,"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "status": 'backtesting'
  } as algo
 
  await db_algo.update(algo)
  return {data: algo, algo:algo_id, algo_result: algo}
}

export async function updateMarketStopLossLimitAlgo (user_id: number, account_id: number, algo_id: number, timeframe_id: number, indicator:number){

  let algo = {
    "id": algo_id,
    "user_id": user_id,
    "account_id": account_id,
    "name": 'my algo market test',
    "timeframe": timeframe_id,
    "markets_min_volume": 0,
    "buy_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":"<=","trigger":30,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}],"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "sell_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":">=","trigger":60,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}], "stop_loss":0.001, "stop_loss_limit":1,"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "status": 'backtesting'
  } as algo
 
  await db_algo.update(algo)
  return {data: algo, algo:algo_id, algo_result: algo}
}


export async function updateMarketAlgoMutliLayers (user_id: number, account_id: number, algo_id: number, timeframe_id: number, indicator:number){

  let algo = {
    "id": algo_id,
    "user_id": user_id,
    "account_id": account_id,
    "name": 'my algo market test',
    "timeframe": timeframe_id,
    "markets_min_volume": 0,
    "buy_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":"<=","trigger":30,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}],"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "sell_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":">=","trigger":60,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":50, "qty_percent": true,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}, {"type":"limit","percent":0.5,"qty":50, "qty_percent": true,"stall": 120, "stall_protect": { "active": false, "current": 0}, "price": 0}, {"type":"limit","percent":1,"qty":100, "qty_percent": true,"stall": 240, "stall_protect": { "active": false, "current": 0}, "price": 0}], "stop_loss":0,"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "status": 'backtesting'
  } as algo
 
  await db_algo.update(algo)
  return {data: algo, algo:algo_id, algo_result: algo}
}

export async function updateLimitAlgo (user_id: number, account_id: number, algo_id: number, timeframe_id: number, indicator:number){
  let algo = {
    "id": algo_id,
    "user_id": user_id,
    "account_id": account_id,
    "name": 'my algo limit test',
    "timeframe": timeframe_id,
    "markets_min_volume": 0,
    "buy_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":"<=","trigger":30,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"limit","percent":0.25,"qty":1, "qty_percent": false,"stall": 120, "stall_protect": { "active": false, "current": 0}, "price": 0}],"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "sell_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":">=","trigger":60,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"limit","percent":0.25,"qty":1, "qty_percent": false,"stall": 120, "stall_protect": { "active": false, "current": 0}, "price": 0}],"stop_loss":0,"trail_trig":0,"trail_gap_reduce":0,"trail_stop":0, "trail":{"active":false,"trigger":0,"stop":0}},
    "status": 'backtesting'
  } as algo
 
  await db_algo.update(algo)
  return {data: algo, user: user_id, account: account_id, algo:algo_id, algo_result: algo}
}

export async function updateTrailAlgo (user_id: number, account_id: number, algo_id: number, timeframe_id: number, indicator:number){
  let algo = {
    "id": algo_id,
    "user_id": user_id,
    "account_id": account_id,
    "name": 'my algo trailing test',
    "timeframe": timeframe_id,
    "markets_min_volume": 0,
    "buy_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":"<=","trigger":30,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"market","percent":0,"qty":1, "qty_percent": false,"stall": 0, "stall_protect": { "active": false, "current": 0}, "price": 0}],"trail_trig":1,"trail_gap_reduce":0,"trail_stop":2, "trail":{"active":false,"trigger":0,"stop":0}},
    "sell_pref": {"indicators":[{"id":indicator,"name": "RSI TEST","trigger_cond":">=","trigger":50,"timeframe":{"id":timeframe_id,"msec_value":0}, "fnc": Function(), "periods":[0]}],"layer_index":0,"layers":[{"type":"limit","percent":1,"qty":100, "qty_percent": true,"stall": 120, "stall_protect": { "active": false, "current": 0}, "price": 0}],"stop_loss":0,"trail_trig":1,"trail_gap_reduce":0,"trail_stop":2, "trail":{"active":false,"trigger":0,"stop":0}},
    "status": 'backtesting'
  } as algo
 
  await db_algo.update(algo)
  return {data: algo, user: user_id, account: account_id, algo:algo_id, algo_result: algo}
}