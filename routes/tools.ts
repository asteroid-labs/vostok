import { test, algo, test_trade  } from '../interfaces/index';

export async function calcInitial(test: test, algo: algo, trades: test_trade[]){
    let initial_base_amount = test.base_amount ? test.base_amount : trades.length > 0 ? (trades[0].qty * trades[0].price) : 0
    if(!algo.buy_pref.layers[0].qty_percent && Number(algo.buy_pref.layers[0].qty) < Number(initial_base_amount)){
        initial_base_amount = Number(algo.buy_pref.layers[0].qty)
    }
    if(algo.buy_pref.layers[0].qty_percent && (Number(algo.buy_pref.layers[0].qty) < 100)){
        initial_base_amount = (Number(algo.buy_pref.layers[0].qty)/100) * test.base_amount
    }

    return initial_base_amount
}

export async function shuffle(string_length: number){
    return Math.floor(Math.random()*string_length)
}