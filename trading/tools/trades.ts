import { complete_test_info } from '../../interfaces/index';
import { setupTradeMsg } from '../../notifications/index'
import { write } from './logs';
import { reset as stallReset }  from './stall'
const debug = false

export async function reset(test: complete_test_info){
    // trade has happened
    // trade.id = 0
    // reset values to give a cooldown time
    test.markets[test.index].trade_info.last.price = 0
    test.markets[test.index].trade_info.price = 0
    test.markets[test.index].trade_info.trail_price = 0
    test.markets[test.index].trade_info.last.qty = 0
/*     test.markets[test.index].trail = {
        active: test.markets[test.index].pref.order_type === 'trailing' ? true : false,
        trigger: Number(test.markets[test.index].pref.trail_trig),
        stop: Number(test.markets[test.index].pref.trail_stop),
    } */
    //test.trade_info.id = 0
    //stall_protect.active = false
    //stall_protect.current = 0 // or pref?
    //layer.stall_protect.current = test.markets[test.index].pref.layers[test.layer_index].stall

    return test
}

export async function recordFakeTrade(test: complete_test_info, side: string){
    //console.log(test.markets[test.index].trade_info.last.qty)
    if(test.markets[test.index].trade_info.last.qty !== 0){
        test = await setupTradeMsg(test, side)
        write(test.markets[test.index].market_data[test.candle.i][0], 
            test.markets[test.index].market.name,
            'trade',
            test.original.id, 
            debug,
            `: trade # ${JSON.stringify(test.markets[test.index].trade_info.id)}, ${side} ${test.markets[test.index].trade_info.last.qty} ${test.markets[test.index].market.name.split("/")[0]} at ${test.markets[test.index].trade_info.last.price} ${test.markets[test.index].market.name.split("/")[1]}`
        )
        test = await reset(test)
        test = await stallReset(test, side)
        //test = await changeState(test);
    }
    return test
}