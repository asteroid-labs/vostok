const debug = false
import { pool } from './index';
import { test } from '../interfaces/index';

export async function create(test: test) {
    let res = {} as any
    let text = 'INSERT INTO tests (time, type, exchange_id, pair_ids, user_id, account_id, algo_id, base_amount, state, start, stop) '+
                'VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) ON CONFLICT DO NOTHING RETURNING *;'
    let values = [
        test.time,
        test.type,
        test.exchange_id,
        test.pair_ids,
        test.user_id,
        test.account_id,
        test.algo_id,
        test.base_amount,
        test.state,
        test.start,
        test.stop
    ]
    try {
        res = await pool.query(text, values) // upsert
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done create test')
        }
    } catch (err) {
        console.log(err.stack, 'error create test', test)
    }
    return res.rows[0] as test
}

export async function update(id: number, state: string) {
    let res = {} as any
    let text = 'UPDATE tests SET state = $1 WHERE id = $2 RETURNING *;'
    let values = [state, id]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update tests')
    }
    return res.rows[0] as test
}

export async function unlink(id: number) {
    let res = {} as any
    let text = 'DELETE FROM test_logs WHERE id = $1;'
    try {
        res = await pool.query(text, [id])
    } catch (err) {
        console.log(err.stack, 'error delete tests_logs', id)
    }
    text = 'DELETE FROM test_trades WHERE test = $1;'
    try {
        res = await pool.query(text, [id])
    } catch (err) {
        console.log(err.stack, 'error delete tests_trades', id)
    }
    text = 'DELETE FROM tests WHERE id = $1;'
    try {
        res = await pool.query(text, [id])
    } catch (err) {
        console.log(err.stack, 'error delete tests', id)
    }
}

export async function get(id: number) {
    let res = {} as any

        try {
            res = await pool.query('SELECT * FROM tests WHERE id = $1;', [id])
        } catch (err) {
            console.log(err.stack, 'error get test')
        }

    return res.rows[0] as test
}

export async function getDanglings() {
    let res = {} as any
        try {
            res = await pool.query(`SELECT * FROM tests WHERE (state != 'done' AND state != 'deleted');`)
        } catch (err) {
            console.log(err.stack, 'error getDanglings test')
        }

    return res.rows as test[]
}

export async function getByUser(id: number, type: string) {
    let res = {} as any

        try {
            res = await pool.query("SELECT * FROM tests WHERE user_id = $1 AND type = $2 AND state != 'deleted' ORDER BY id DESC;", [id, type])
        } catch (err) {
            console.log(err.stack, 'error get test')
        }

    return res.rows as test[]
}

export async function getRunningByAlgo(id: number, type: string) {
    let res = {} as any

        try {
            res = await pool.query("SELECT * FROM tests WHERE algo_id = $1 AND type = $2 AND state != 'deleted' AND state != 'done';", [id, type])
        } catch (err) {
            console.log(err.stack, 'error get test')
        }

    return res.rows as test[]
}

export async function getAllByAlgo(id: number) {
    let res = {} as any

        try {
            res = await pool.query("SELECT * FROM tests WHERE algo_id = $1 AND state != 'deleted';", [id])
        } catch (err) {
            console.log(err.stack, 'error get test')
        }

    return res.rows as test[]
}