import { algo_indicator, complete_test_info } from '../../interfaces/index';
import * as db_indicator from '../../db/indicator';
import * as db_timeframe from '../../db/timeframe';
import { getLastCandles } from './timeframes';
import tulind_indicators from '../indicators/tulind/index'
import { write } from './logs';

const debug = false

export async function convertIndicator(test: complete_test_info){
    // converts indicator id to name and match it to the imported tulind function, returning an array of fncs
    for(const pref of [test.algo.buy_pref, test.algo.sell_pref]){
        for (const indicator of pref.indicators){
            let db_indi = await db_indicator.getOne(indicator.id)
            let tulind = {} as any 
            try{
                tulind = tulind_indicators.find(({ name }) => name === db_indi.function)
            }catch(err){
                console.log(err, 'no indicator function', indicator)
            }
            indicator.periods = [0]
            if(tulind){
                indicator.fnc = tulind.fnc
                indicator.name = db_indi.name
                indicator.periods = db_indi.periods
            }
            let res = await db_timeframe.getOne(indicator.timeframe.id)
            if(typeof res !== 'undefined'){
                indicator.timeframe.msec_value = Number(res.msec_value)
            }
        }
    }
    return test
}

export async function fetchNewMarketData(test: complete_test_info){
    // find the indicator who need the most 1m candles
    let max = 0
    for(const pref of [test.algo.buy_pref, test.algo.sell_pref]){
        for (const indicator of pref.indicators){
            let time_gap = (Number(indicator.timeframe.msec_value) / 60000)
            let temp_max = (10 * Math.max.apply(Math, indicator.periods)) * time_gap
            if(temp_max > max){
                max = temp_max
            }
        }
    }
    //debug ? console.log('maximum candle needed is', max) : false
    if(test.markets[test.index].market_data.length > max){
        // keep only the last 'max' in 1m candles
        test.markets[test.index].market_data = test.markets[test.index].market_data.slice(test.markets[test.index].market_data.length - max, test.markets[test.index].market_data.length)
    }
    //fetch new candles
    let last_candles = await getLastCandles(test.markets[test.index].market.sputnik_id, 60000, 1)
    // verify that it's not already in the array
    let id = last_candles.length > 0 ? last_candles.length - 1 : 0
    if(typeof last_candles[id] !== 'undefined' && last_candles[id][0] > test.markets[test.index].market_data[test.markets[test.index].market_data.length-1][0]){
        test.markets[test.index].market_data.push(last_candles[id])
        //console.log('diff candle')
    }else if(typeof last_candles[id] !== 'undefined' && last_candles[id][0] === test.markets[test.index].market_data[test.markets[test.index].market_data.length-1][0]){
        //console.log('same candle', last_candles[id])
        // need to update values if price or volume changed
        test.markets[test.index].market_data[test.markets[test.index].market_data.length-1][0] = last_candles[id][0]
    }
    //debug ? console.log('data length is:', test.markets[test.index].market_data.length):false
    return test
}

export async function testTriggers(test: complete_test_info, side: string){
    let result = false
    let triggers = [] as boolean[]
    let indicators = [] as algo_indicator[]
    if(side === 'buy'){
        indicators = test.algo.buy_pref.indicators
    }else{
        indicators = test.algo.sell_pref.indicators
    }
    try{
        for (const [j, indicator] of indicators.entries()){
            // this calls the corresponding tulind with "up to this" data
            
            // need to take into account the gap of indicators 
            if(indicator.fnc && indicator.timeframe.msec_value){
                let time = test.markets[test.index].market_data[test.candle.i][0] // in ms
                time = (time-(time%1000))/1000 // in seconds
                let indicator_time = (indicator.timeframe.msec_value-(indicator.timeframe.msec_value%1000))/1000 // in seconds
                let k = time / indicator_time
                if(Number.isInteger(k)){
                    //debug ? console.log('its a minute candle!') : false
                    k = test.candle.i
                    // k should represent the test.markets[test.index].market_data (index +1) of the current candle
                    // market data is in 60000 bundles, we need to convert it to msec_value bundles
                    let indicator_data = await convertData(test.markets[test.index].market_data, indicator.timeframe.msec_value, k)
                    triggers = await runIndicator(test, indicator_data, indicator, triggers, side)
                }else{
                    //debug ? console.log('waiting for a candle that fits the indicator', indicator_time, k) : false
                }
            }else {
                write(test.markets[test.index].market_data[test.candle.i][0],
                    test.markets[test.index].market.name,
                    'indicator_'+side,
                    test.original.id,
                    debug,
                    `: **** An indicator (id: ${indicator.id}, periods: ${indicator.periods}, data:${test.markets[test.index].market_data.length}) has configuration problems **** `
                )
            }
        }   
        if(triggers.length > 0 && triggers.every((a) => a === true)){
            result = true
        }
    }catch(err){
        console.log(err, 'error test trigger', indicators)
    }
    return result
}

async function convertData(market_data: number[][], msec_value: number, k: number){
    // slice market_data to k 
    let temp_data = market_data.slice(0, k) // keep the first 120 candles

    // with the time gap (msec_value / 60000), convert (slice last time_gap candles (ex, 15)) to msec_value bundles
    let time_gap = (msec_value / 60000) // = 60 for 1h

    let final_bundles = [[0]]
    for(const [i, candle] of temp_data.entries()){
        if(i === 0){
            final_bundles[0] = candle
        }else{
            if(i%time_gap !== 0){
                // continue building bundle
                if(candle[2] > final_bundles[final_bundles.length - 1][2]){
                    final_bundles[final_bundles.length - 1][2] = candle[2] // updating high
                }
                if(candle[3] < final_bundles[final_bundles.length - 1][3]){
                    final_bundles[final_bundles.length - 1][3] = candle[3] // updating low
                }
                final_bundles[final_bundles.length - 1][4] = candle[4] // updating close
                final_bundles[final_bundles.length - 1][5] = (final_bundles[final_bundles.length - 1][5] + candle[5]) // updating volume 
            }else{
                // this is a new bundled candle, advancing index
                final_bundles.push(candle)
            }
        }
        
    }

    // return bundled data
    //debug ? console.log('new bundle created') : false
    return final_bundles
}

async function runIndicator(test: complete_test_info, indicator_data: number[][], indicator:algo_indicator, triggers: boolean[], side: string ){
    let calc_result = [] as number[]
    if(typeof indicator.fnc !== 'undefined'){ // will only calculate on tf candles even if i is different
        try{
            calc_result = await indicator.fnc(indicator_data, indicator.periods) //.slice(0, k+1)
        }catch(err){
            console.log(err, 'error testing indicator', indicator)
        }
        // last element in the newest
        //console.log(calc_result[calc_result.length-1])
        if(typeof calc_result[calc_result.length-1] === 'number'){
            // warmup phase has passed
            let result = await compareToTrigger(indicator.trigger_cond, indicator.trigger, calc_result[calc_result.length-1])
            triggers.push(result)
            let timeframe = ''

            switch (indicator.timeframe.msec_value) {
                case 60000:
                    timeframe = '1m'
                break;
                case 300000:
                    timeframe = '5m'
                break;
                case 900000:
                    timeframe = '15m'
                break;
                case 1800000:
                    timeframe = '30m'
                break;
                case 3600000:
                    timeframe = '1h'
                break;
                case 7200000:
                    timeframe = '2h'
                break;
                case 14400000:
                    timeframe = '4h'
                break;
                case 21600000:
                    timeframe = '6h'
                break;
                case 43200000:
                    timeframe = '12h'
                break;
                case 86400000:
                    timeframe = '1D'
                break;
                case 604800000:
                    timeframe = '1W'
                break;
                case 2628002880:
                    timeframe = '1M'
                break;
            }

            write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                'indicator_'+side,
                test.original.id,
                debug,
                `: The ${indicator.name} indicator for ${timeframe} is at ${calc_result[calc_result.length-1].toFixed(0) === '0' ? calc_result[calc_result.length-1] : calc_result[calc_result.length-1].toFixed(2)}, trigger condition is ${indicator.trigger_cond} ${indicator.trigger}`
            )
        }else{
            debug ? console.log('Indicator is warming up') : false
         /*     write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                test.type,
                test.original.id,
                debug,
                `: ${test.markets[test.index].market_data[test.candle.i][0]} warmup ${calc_result}, ${k}, ${indicator.timeframe.market_data.length}`
            )  */
        }
    }
    return triggers
}

export async function compareToTrigger(condition: string, trigger: number, calc_result: number){
    // converts string condition to =>< and result true/false if the trigger is matched
    let result = false
    if(Number(calc_result) !== 0){
        switch (condition){
            case '=':
                if(parseFloat(calc_result.toString()) === parseFloat(trigger.toString())){
                    result = true
                }
                break
            case '>':
                if(parseFloat(calc_result.toString()) > parseFloat(trigger.toString())){
                    result = true
                }
                break
            case ('>=' || '=>'):
                if(parseFloat(calc_result.toString()) >= parseFloat(trigger.toString())){
                    result = true
                }
                break
            case '<':
                if(parseFloat(calc_result.toString()) < parseFloat(trigger.toString())){
                    result = true
                }
                break
            case ('<=' || '=<'):
                if(parseFloat(calc_result.toString()) <= parseFloat(trigger.toString())){
                    result = true
                }
                break
        }
    }
    //console.log(calc_result, condition, trigger)
    return result
}