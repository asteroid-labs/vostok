/* ALTER TABLE users
    ALTER COLUMN prefs TYPE JSONB;
ALTER TABLE accounts
    ALTER COLUMN prefs TYPE JSONB;
ALTER TABLE algos
    ALTER COLUMN buy_pref TYPE JSONB,
    ALTER COLUMN sell_pref TYPE JSONB;
ALTER TABLE indicators
    ALTER COLUMN functions TYPE JSONB; */
--DELETE FROM indicators; DROP TABLE indicators;
--DELETE FROM test_trades; DROP TABLE test_trades;
--DELETE FROM balances; DROP table balances;
--DELETE FROM test_trades; DROP table test_trades;
DELETE FROM test_logs; DROP table test_logs;
--DELETE FROM tests; DROP table tests; 
