import express from 'express';
import * as db_timeframe from '../db/timeframe';
import * as config from '../config.json';
import { user, timeframe } from '../interfaces/index';


let router: express.Router = express.Router();

router.get('', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            let admin = true
            let timeframes = await db_timeframe.getAll()
            res.render('pages/timeframes.ejs', { admin, timeframes })
        } else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
})

router.post('/:timeframe_id', async function(req, res) {
    if (req.isAuthenticated()) {
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
        const timeframe_id = Number(req.params.timeframe_id)
        
        if (req.body.delete_timeframe === '') {
            let del = await db_timeframe.unlink(timeframe_id)
            res.redirect('back')
        } else {
            // save timeframe's config
            let timeframe = {
                id: timeframe_id,
                name: req.body.name,
                msec_value: Number(req.body.mseq_value)
            } as timeframe
            await db_timeframe.update(timeframe)
            res.redirect('/timeframe/' + timeframe_id)
        }
        } else {
            // not logged in
            res.redirect('/login')
        }
    } else {
        // not logged in
        res.redirect('/login')
    }
})

router.get('/new', async function(req, res) {
    if (req.isAuthenticated() && req.user) {
        const user = req.user as user
            if (user.email.toLowerCase() === config.web.email) {
                let timeframe = {
                    name: 'indicator name',
                    msec_value: 60000,
                } as timeframe
                let tf = await db_timeframe.create(timeframe)
                res.redirect('/timeframe/' + tf.id)
            } else {
                // not logged in
                res.redirect('/login');
            }
    } else {
        res.redirect('/login');
    }
})

router.get('/:timeframe_id', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        const user = req.user as user
            if (user.email.toLowerCase() === config.web.email) {
                let admin = true
                const timeframe_id = Number(req.params.timeframe_id)
                let timeframes = await db_timeframe.getOne(timeframe_id)
                res.render('pages/timeframes.ejs', {auth, admin, timeframes })
            } else {
                // not logged in
                res.redirect('/login');
            }
    } else {
        res.redirect('/login');
    }
})

export = router
