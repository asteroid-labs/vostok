# Vostok

Back-end trading engine using front-end build algorithms. First manned mission into space.

## Preparation

* setup docker container for timescaledb `timescaledb-vostok`

* run `pnpm install && pnpm run db` 

* copy config_exanple.json to config.json and fill in values

* using `psql`, create the vostok user and vostok database

* `pnpm start` will compile typescript, create tables and start webserver


## Scope

* Vostok consists of web pages used to create trading algorithms and backtest them on sputnik's data.

* Fake trades will be recorded in the DB and can be compared and gains extracted for leaderboards.


## TODO

* database version table to drop if table updates
* prepare database following the drawio (users, accounts, algo, trades)
* do all scaffolding and backtesting codes (create user, add/edit/remove account,algo, fake trades)
* modify page UI to edit users, accounts and algos better
* create a webpage to create users, user's accounts, user's algo
* run user's algo on backtesting

## User Workflow

* Create a user workflow and plan it (drawio)

* page to sign-up/login, send sign-up by email with verif and 2fa
