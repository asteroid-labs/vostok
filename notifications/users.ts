const api = require('binance')
import * as db_user from '../db/user';
import * as db_account from '../db/account';
import * as db_balance from '../db/real_balance';
import { account, real_balance } from 'interfaces';
import { setupNotifMsg } from './index';
import { complete_test_info, market, trade_info, execution_notif, user } from '../interfaces/index';

const debug = false

process.on('message', (m) => {
    //console.log(m)
    switch (m.m) {
        case 'start':
            queryUsers()
            break;
    
        case 'queryAccounts':
            queryAccounts(m.queryAccounts)
            break;

        case 'startAccountWS':
            startAccountWS(m.startAccountUser, m.startAccountWS)
            break;
    }
  });

async function queryUsers(){
    // for every account, if telegramid is set -> query all it'S account, for each where notifs is on, start ws
    // find a way to loop this or stop/start
    let users = await db_user.getAllUser()
    for (const user of users){
        if(user.prefs?.telegram_id != ''){
            // this user has set it's telegram id
            queryAccounts(user)
            //console.log('query account')
        }else{
            //console.log(user.prefs)
        }
    }
}

async function queryAccounts(user: user){
    let accounts = await db_account.getAll(user.id, false)
    for (const account of accounts){
        if(account.prefs){
            let prefs = account.prefs as any
            if(prefs.telegram_notifications === 'on' && account.api_key != '' && account.api_key != "Your Exchange API Key"){
                startAccountWS(user, account)
                console.log('USER #', user.id, 'Started account', account.name)
            }else{
                //console.log(account.prefs)
            }
        }
    }
}

async function startAccountWS(user: user, account: account){
    // start binance ws and call the appropriate functions
    const binanceRest = new api.BinanceRest({
        key: account.api_key,
        secret: account.api_secret, // Same for this
        timeout: 15000, // Optional, defaults to 15000, is the request time out in milliseconds
        recvWindow: 5000, // Optional, defaults to 5000, increase if you're getting timestamp errors
        disableBeautification: false,
        handleDrift: true
      })
    const binanceWS = new api.BinanceWS(true)
    try {
        let ws = await binanceWS.onUserData(binanceRest, async (data:any) => {
           
            //console.log(data)
            switch (data.eventType || data.e) {
                case 'executionReport':
                    data = data as execution_notif
                    // order executed
                    /* {
                        eventType: 'executionReport',
                        eventTime: 1513808673916,
                        symbol: 'IOTABTC',
                        newClientOrderId: '81gmMRozYdU73D27Ho1W1K',
                        side: 'SELL',
                        orderType: 'LIMIT',
                        cancelType: 'GTC',
                        quantity: '10.00000000',
                        price: '0.00030120',
                        stopPrice: '0.00000000',
                        icebergQuantity: '0.00000000',
                        g: -1, // to be ignored
                        originalClientOrderId: 'null',
                        executionType: 'TRADE',
                        orderStatus: 'FILLED',
                        rejectReason: 'NONE',
                        orderId: 9409314,
                        lastTradeQuantity: '10.00000000',
                        accumulatedQuantity: '10.00000000',
                        lastTradePrice: '0.00030120',
                        commission: '0.00000301',
                        commissionAsset: 'BTC',
                        tradeTime: 1513808673912,
                        tradeId: 3023119,
                        I: 21799081, // ignore
                        w: false, // ignore
                        maker: true
                    }*/
                    // get market
                    let symbol = ''
                    if(data.symbol.slice(-3) === 'BTC'){
                        data.symbol = data.symbol.replace(/BTC/gi, "/BTC")
                    }else if(data.symbol.slice(3) === 'BTC'){
                        data.symbol = data.symbol.replace(/BTC/gi, "BTC/")
                    }
                    if(data.symbol.slice(-4) === 'USDT'){
                        data.symbol = data.symbol.replace(/USDT/gi, "/USDT")
                    }else if(data.symbol.slice(4) === 'USDT'){
                        data.symbol = data.symbol.replace(/USDT/gi, "USDT/")
                    }

                    let test = {
                        original: {
                            type: 'real-notif',
                            time: Number(data.eventTime),
                            user_id: account.user_id,
                            account_id: account.id,
                        },
                        index: 0,
                        markets: [
                            {
                                market: { name: data.symbol } as market,
                                trade_info: {
                                    state: data.side === 'BUY' ? 'buy' : 'sell',
                                    id: 0,
                                    last: {
                                        price: parseFloat(data.lastTradePrice), 
                                        qty: parseFloat(data.quantity)
                                    },
                                    price: parseFloat(data.lastTradePrice),
                                    base_amount_override: 0,
                                    trail_price: 0
                                } as trade_info,
                                market_data:[[parseFloat(data.lastTradePrice)]]
                            }
                        ],
                        candle:{i:0},
                        type: 'real',
                    }as complete_test_info

                    //console.log('1',test)
                    if(data.orderType !== 'MARKET' || data.orderStatus === 'FILLED'){
                        await setupNotifMsg(test, user, account, data)
                        //console.log(test)
                    }else{
                        
                    }
                    //console.log('2',test)
                    
                    switch (data.side) {
                        case 'BUY':
                                // buy order executed
                                //console.log('buy order executed', data)
                            break;
                    
                        case 'SELL':
                                // sell order executed
                                //console.log('sell order executed', data)
                            break;
                    }
                    break;
            
                case 'outboundAccountInfo':
                    /* {
                        eventType: 'outboundAccountInfo',
                        eventTime: 1513808673916,
                        makerCommission: 10,
                        takerCommission: 10,
                        buyerCommission: 0,
                        sellerCommission: 0,
                        canTrade: true,
                        canWithdraw: true,
                        canDeposit: true,
                        lastUpdateTime: 1499405658848,
                        balances: [
                            {
                                asset: 'BTC',
                                availableBalance: '0.00301025',
                                onOrderBalance: '0.00000000'
                            },
                            ...
                        ]
                    }
                    */
                   if(data.B?.length > 0){
                    for(const b of data.B){
                            let balance = {
                                account_id: account.id,
                                symbol: b.asset,
                                free: parseFloat(b.availableBalance),
                                locked: parseFloat(b.onOrderBalance)
                            }
                            await db_balance.upsert(balance)
                        }
                    }
                    //console.log(data);
                break;
                case 'outboundAccountPosition':
                    // {
                    //     e: 'outboundAccountPosition',
                    //     E: 1582488065986,
                    //     u: 1582488065984,
                    //     B: [
                    //       { a: 'BTC', f: '0.00169049', l: '0.00000000' },
                    //       { a: 'BNB', f: '0.00000000', l: '0.00000000' },
                    //       { a: 'ADA', f: '0.79400000', l: '0.00000000' }
                    //     ]
                    //   }
                    if(data.B?.length > 0){
                        for(const b of data.B){
                            let balance = {
                                account_id: account.id,
                                symbol: b.a,
                                free: parseFloat(b.f),
                                locked: parseFloat(b.l)
                            }
                            await db_balance.upsert(balance)
                        }
                    }
                break;
            }
        }, 60000) // Optional, how often the keep alive should be sent in milliseconds
    }catch(err){
        console.log(err, 'account notif', account)
    }
}
