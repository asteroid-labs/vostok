import { pool } from './index';
import * as db_timeframe from './timeframe';
import * as db_indicator from './indicator';
import * as db_account from './account';
import { algo } from '../interfaces/index';

const debug = false

export async function create(algo: algo) {
    let res = {} as any
    if (algo.user_id) {
        if(algo.account_id === 0){
            let text = 'INSERT INTO algos (user_id, name, markets_min_volume, buy_pref, sell_pref, status) '+
                        'VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT DO NOTHING RETURNING id;'
            let values = [algo.user_id,
                algo.name,
                algo.markets_min_volume,
                JSON.stringify(algo.buy_pref),
                JSON.stringify(algo.sell_pref),
                algo.status
            ]
            try {
                res = await pool.query(text, values) // upsert
            } catch (err) {
                console.log(err.stack, 'error no account algo', algo)
            }
        }else{
            try {
                let text = 'INSERT INTO algos (user_id, account_id, name, markets_min_volume, buy_pref, sell_pref, status) '+
                            'VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT DO NOTHING RETURNING id;'
                let values = [algo.user_id,
                    algo.account_id,
                    algo.name,
                    algo.markets_min_volume,
                    JSON.stringify(algo.buy_pref),
                    JSON.stringify(algo.sell_pref),
                    algo.status
                ]
                res = await pool.query(text, values) // upsert
            } catch (err) {
                console.log(err.stack, 'error algo', algo)
            }
        }
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done save algo')
        }
        
    }
    return res.rows[0] as algo
}

export async function update(algo: algo) {
    let res = {} as any
    let text = 'UPDATE algos SET account_id = $1, name = $2, markets_min_volume = $3, '+
                'buy_pref = $4, sell_pref = $5, status = $6 WHERE id = $7;'
    let values = [algo.account_id,
                algo.name,
                algo.markets_min_volume,
                JSON.stringify(algo.buy_pref),
                JSON.stringify(algo.sell_pref),
                algo.status,
                algo.id
            ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update algo')
    }
    return true
}

export async function unlink(user_id: number, id: number) {
    let res = {} as any
    let text = "UPDATE algos SET status = 'deleted' WHERE user_id = $1 AND id = $2;"
    let values = [
        user_id, id
    ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error delete algo')
    }
}

export async function getOne(user_id: number, account_id: number, algo_id: number, test = false) {
    let res = {} as any
    let text = 'SELECT * FROM algos WHERE user_id = $1 AND account_id = $2 AND id = $3;'
    let values = [
        user_id,
        account_id,
        algo_id
    ]
    
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error get algo')
    }
    if(res.rows.length > 1 && !test){
        res.rows[0].timeframes = await db_timeframe.getAll()
        res.rows[0].indicators = await db_indicator.getAll()
    }
    return res.rows[0] as algo
}

export async function getOneFromUser(user_id: number, algo_id: number, test = false) {
    let res = {} as any
    let text = "SELECT * FROM algos WHERE user_id = $1 AND id = $2 AND status != 'deleted';"
    let values = [
        user_id,
        algo_id
    ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error get algo')
    }
    if(res.rows.length > 1 && !test){
        res.rows[0].timeframes = await db_timeframe.getAll()
        res.rows[0].indicators = await db_indicator.getAll()
        res.rows[0].accounts = await db_account.getAll(user_id, false)
    }
    return res.rows[0] as algo
}


export async function getAllFromUser(user_id: number) {
    let res = {} as any
    let text = "SELECT * FROM algos WHERE user_id = $1 AND status != 'deleted' ORDER BY id ASC;"
    let values = [
        user_id
    ]
    let timeframes = await db_timeframe.getAll()
    let indicators = await db_indicator.getAll()
    let accounts = await db_account.getAll(user_id, false)

    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error get algo')
    }
    for (const row of res.rows) {
        row.timeframes = timeframes
        row.indicators = indicators
        row.accounts = accounts
    }
    
    return res.rows as algo[]
}

export async function getAllFromAccount(user_id: number, account_id: number) {
    let res = {} as any
    let text = "SELECT * FROM algos WHERE user_id = $1 AND account_id = $2 AND status != 'deleted' ORDER BY id ASC;"
    let values = [
        user_id,
        account_id
    ]
    let timeframes = await db_timeframe.getAll()
    let indicators = await db_indicator.getAll()

    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error get algo')
    }
    for (const row of res.rows) {
        row.timeframes = timeframes
        row.indicators = indicators
    }
    return res.rows as algo[]
}