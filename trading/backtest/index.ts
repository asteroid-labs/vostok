import * as db_test from '../../db/tests';
import { test, complete_test_info } from '../../interfaces/index';
import child from 'child_process';
import * as db_test_logs from '../../db/test_logs';
import * as db_test_trade from '../../db/test_trade';
/* tools */
import { convertIndicator} from '../tools/indicators';
import { initiateTest, endTest} from '../tools/initiate';

/* trading loop */
import { tradeLoop } from '../strategies';

let debug = false

process.on('message', async function(msg) {
    //console.log('Message from parent:', msg);
    if(msg.test_id){
        try{
            await initiateTest(msg.test_id, 'backtest', startStrategies)
        }catch(err){
            console.log(err.stack, 'error on backtest main()', msg.test_id)
        }
    }
    process.exit()
  });
  
export async function test(id: number){
    try{
        await initiateTest(id, 'backtest', startStrategies)
    }catch(err){
        console.log(err.stack, 'error on backtest test()', id)
    }
}

async function startStrategies(test: complete_test_info){
    // this fnc will determine the appropriate trading strategy and call small fncs every candles

    let restart = false
    test.candle.i = 0
    test = await convertIndicator(test)
    for (const [i, candle] of test.markets[0].market_data.entries()){
        //console.log(1, test.original)
        test.original = await db_test.get(test.original.id)
        //console.log(2, test.original)
        if(test.original.state === 'running'){
            for(const [j, market] of test.markets.entries()){
                test.candle.i = i
                if(Number(candle[0]) > market.last_trade_time){
                    test.index = j
                    //trade fnc
                    test = await tradeLoop(test)
                }
            }
        }else if (test.original.state === 'restart'){
            debug ? console.log('restarting from loop') : false
            restart = true
        }
    }
    // candles ended, save a done side
    if(restart){
        debug ? console.log('restaring after loop') : false
        await db_test_logs.unlinkAll(test.original.id)
        debug ? console.log('removing logs') : false
        await db_test_trade.unlinkChilds(test.original.id)
        debug ? console.log('removing trades') : false
        await db_test.update(test.original.id, 'init')
        try {
            let forked = child.fork(`./build/trading/${test.type}/index.js`)
            forked.on('message', (msg) => {
                console.log(`Message from ${test.type}er: "` + msg + '"')
            })
            forked.send({ test_id : test.original.id })
        } catch (err) {
            console.log(err.stack, 'forking test')
        }
    }else{
        await endTest(test)
    }

    return 0
}