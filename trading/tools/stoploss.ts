import { complete_test_info, algo_layers } from "interfaces";
import * as db_trade from '../../db/test_trade'
import { MarketSellLayer } from '../layers/market';
import { LimitSellLayer } from '../layers/limit';
import { write } from '../tools/logs';

const debug = false

export async function verifyStopLoss(test: complete_test_info){
    if(test.algo.sell_pref.stop_loss !== 0){
        //console.log('stop-loss activated')
        //stop-loss is activated
        //check last buy price
        let last_trade = await db_trade.getLast(test.original.id, 'buy')
        //if candle <= last_buy - stop_loss %
            //set limit config following limit price
            //or market sell for now
        let stop_loss = typeof test.algo.sell_pref.stop_loss !== 'undefined' ? test.algo.sell_pref.stop_loss : 0
        //console.log('last trade', last_trade, test.markets[test.index].market_data[test.candle.i][1], (last_trade.price * (1 - (stop_loss / 100))))
        if(last_trade && test.markets[test.index].market_data[test.candle.i][1] <= (last_trade.price * (1 - (stop_loss / 100)))){
            //this candle open is lower than specified stop_loss, sell
            if(test.algo.sell_pref.stop_loss_limit === 0){
                //console.log('stop-loss is market')
                write(test.markets[test.index].market_data[test.candle.i][0],
                    test.markets[test.index].market.name,
                    'stop-loss',
                    test.original.id,
                    debug,
                    `: sell stop-loss triggered ( - ${test.algo.sell_pref.stop_loss}% from ${last_trade.price.toFixed(8)}), market selling.`
                )
                test = await MarketSellLayer(test, true);
            }else if(typeof test.algo.sell_pref.stop_loss_limit !== 'undefined'){
                //console.log('stop-loss is limit')
                write(test.markets[test.index].market_data[test.candle.i][0],
                    test.markets[test.index].market.name,
                    'stop-loss',
                    test.original.id,
                    debug,
                    `: sell stop-loss triggered ( - ${test.algo.sell_pref.stop_loss}% from ${last_trade.price.toFixed(8)}), limit selling ${test.algo.sell_pref.stop_loss_limit}% ${test.algo.sell_pref.stop_loss_limit > 0 ? 'higher' : 'lower'}.`
                )
                let limit_layer = {
                    type: 'limit', 
                    percent: test.algo.sell_pref.stop_loss_limit,
                    qty: 100,
                    qty_percent: true,
                    stall: 0,
                    stall_protect: {active: false, current: 0},
                    price: 0,
                } as algo_layers
                let temp_layer = test.algo.sell_pref.layer_index
                test.algo.sell_pref.layers.push(limit_layer)
                test.algo.sell_pref.layer_index = test.algo.sell_pref.layers.length - 1

                test = await LimitSellLayer(test, true);
                
                test.algo.sell_pref.layers = test.algo.sell_pref.layers.splice(-1, 1)
                test.algo.sell_pref.layer_index = temp_layer
            }
        }
    }
    return test
}