import { pool } from './index';
import { test_balance } from '../interfaces/index';
const debug = false

export async function upsert(balance: test_balance) {
    let res = {} as any
    let text = 'INSERT INTO balances (test_id, symbol, free, locked) '+
                'VALUES ($1, $2, $3, $4) ON CONFLICT ON CONSTRAINT unique_balances_con '+
                'DO UPDATE SET (free, locked) = ($3, $4) RETURNING test_id, symbol, free, locked;'
    let values = [
        balance.test_id,
        balance.symbol,
        balance.free,
        balance.locked
    ]
    try {
        res = await pool.query(text, values) // upsert
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done save test_balance')
        }
    } catch (err) {
        console.log(err.stack, 'error upsert test_balance', balance)
    }
    return res.rows[0] as test_balance
}

export async function getOne(test_id: number, symbol: string) {
    let res = {} as any
    let text = 'SELECT * FROM balances WHERE test_id = $1 AND symbol = $2;'
    let values = [test_id, symbol]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error get test_balances')
    }
    let balance = {
            test_id: test_id,
            symbol: symbol,
            free:0,
            locked:0
        } as test_balance
    if(res.rows.length === 0){
        balance = await upsert(balance)
        console.log('no test_balance exists', res.rows, balance)
    }else{
        balance = res.rows[0]
    }
    return balance
}

export async function unlinkAll(test_id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM balances WHERE test_id = $1;', [test_id])
    } catch (err) {
        console.log(err.stack, 'error unlinkall test_balances')
    }
    return 'done!'
}