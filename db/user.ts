import * as bcrypt from 'bcryptjs';
import * as db_account from './account';
const debug = false
import { pool } from './index';
import { user } from '../interfaces/index';


export async function admin(user: user) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM users WHERE email = $1', [user.email])
    } catch (err) {
        console.log(err.stack, 'error create admin user')
    }
    if ((res.rows.length === 0) && user.password) {

        bcrypt.hash(user.password, 8, async function(err, hash) {
            try {
                res = await pool.query('INSERT INTO users (email, password) VALUES ($1, $2) ON CONFLICT DO NOTHING;', [user.email, hash]) // upsert
                if (debug && res.rows.length > 0) {
                    console.log(res.rows[0], 'done save user')
                }
            } catch (err) {
                console.log(err.stack, 'error user')
            }
        });
    }
}

export async function create(user: user) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM users WHERE email = $1', [user.email])
    } catch (err) {
        console.log(err.stack, 'error create user')
        return err
    }

    if (res.rows.length === 0 && user.password) {
        const hashedPassword = await bcrypt.hash(user.password, 8)
        try {
            res = await pool.query('INSERT INTO users (email, password) VALUES ($1, $2) ON CONFLICT DO NOTHING;', [user.email, hashedPassword]) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save user')
            }
        } catch (err) {
            console.log(err.stack, 'error user')
            return err
        }
    } 
    return res.rows[0] as user
}

export async function update(user: user) {
    let res = {} as any
    let text = 'UPDATE users SET name = $1, prefs = $2 WHERE id = $3 RETURNING *;'
    let values = [
        user.name,
        user.prefs,
        user.id
    ]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update user')
        return err
    }
    return res.rows[0] as user
}


export async function validPassword(password: string, hash: string) {
    bcrypt.compare(password, hash, function(err, res) {
        // res === true
        return res
    });

}

export async function find(username: string) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * from users where email = $1', [username])
    } catch (err) {
        console.log(err.stack)
        return err
    }
    return res.rows[0] as user
}

export async function unlink(id: number, username: string) {
    let res = {} as any
    try {
        res = await pool.query('DELETE from users where email = $1 AND id = $2;', [username, id])
    } catch (err) {
        console.log(err.stack)
        return err
    }
    return true
}

export async function getUser(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * from users where id = $1', [id])
    } catch (err) {
        console.log(err.stack)
        return err
    }
    res.rows[0].accounts = await db_account.getAll(id)
    return res.rows[0] as user
}

export async function getAllUser() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * from users`)
    } catch (err) {
        console.log(err.stack)
        return err
    }
    return res.rows as user[]
}