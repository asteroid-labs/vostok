import { complete_test_info, algo_layers, test_balance, real_balance } from '../../interfaces/index';
import * as db_account from '../../db/account';
import * as db_test_balance from '../../db/test_balance';
import * as db_real_balance from '../../db/real_balance';
import { write } from './logs'
import { Decimal } from 'decimal.js';
import { updateAllBalances, fetchMarkets } from '../../binance'

const debug = false

async function applyFilters(test: complete_test_info, qty: number, price: number, side: string){
    let markets = await fetchMarkets()
    let layer = {} as algo_layers
    if(side === 'buy'){
        layer = test.algo.buy_pref.layers[test.algo.buy_pref.layer_index]
    }else{
        layer = test.algo.sell_pref.layers[test.algo.sell_pref.layer_index]
    }
    // check algo amount
    if(!layer.qty_percent){
        // qty should be in quote currency already, we need to compare to base value
        if((layer.qty / test.markets[test.index].trade_info.last.price) <= qty ){
            qty = (layer.qty / test.markets[test.index].trade_info.last.price)
        }
    }else{
  /*       write(test.markets[test.index].market_data[test.candle.i][0],
            test.markets[test.index].market.name,
            test.type,
            test.original.id,
            debug,
            `: using stack % number: ${test.markets[test.index].pref.stack}`
        ) */
    }

    // use % is set
    if(layer.qty_percent && layer.qty <= 100){
        qty = qty * (layer.qty/100)
    }else{
        // log wrong percent number
/*         write(test.markets[test.index].market_data[test.candle.i][0],
            test.markets[test.index].market.name,
            test.type,
            test.original.id,
            debug,
            `: wrong stack % number: ${test.markets[test.index].pref.stack}`
        ) */
    }

    let overflow = 0
    // check amount
    if(qty >= markets[test.markets[test.index].market.name].limits.amount.min){
        // amount seems good
        if(qty <= markets[test.markets[test.index].market.name].limits.amount.max){
            // amount perfect
        }else{
            // amount is too high, correcting
            
            overflow = qty - markets[test.markets[test.index].market.name].limits.amount.max
            //console.log('overflow', qty, markets[test.markets[test.index].market.name].limits.amount.max, overflow)
            qty = qty - overflow
        }
    }else{
        // amount is too low, cannot trade
        // need to log this
        if(qty > 0){
            write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                'balance_'+side,
                test.original.id,
                debug,
                `: calculated amount (${qty}) is too low, the minimum on this market is ${markets[test.markets[test.index].market.name].limits.amount.min}`
            )
            qty = -1
        }
    }

    // check price 
    if(price >= markets[test.markets[test.index].market.name].limits.price.min){
        // price seems good
        if(price <= markets[test.markets[test.index].market.name].limits.price.max){
            // price perfect
        }else{
            // price is too high, correcting
            overflow = price - markets[test.markets[test.index].market.name].limits.price.max
            price = price - overflow
        }
    }else{
        // price is too low, cannot trade
        // need to log this
        if(price > 0){
            write(test.markets[test.index].market_data[test.candle.i][0],
            test.markets[test.index].market.name,
            'balance_'+side,
            test.original.id,
            debug,
            `: calculated price (${price}) is too low, the minimum on this market is ${markets[test.markets[test.index].market.name].limits.price.min}`
            )
            price = 0
        }
        
    }

    // check cost
    if((qty * price) >= markets[test.markets[test.index].market.name].limits.cost.min){
        // price seems good
        // not max cost price
    }else{
        // cost is too low, cannot trade
        // need to log this
        if(qty > 0 && price > 0){
           write(test.markets[test.index].market_data[test.candle.i][0],
            test.markets[test.index].market.name,
            'balance_'+side,
            test.original.id,
            debug,
            `: calculated cost (${(qty * price)}) is too low, the minimum on this market is ${markets[test.markets[test.index].market.name].limits.cost.min}`
            ) 
            qty = 0
            price = 0
        }
        
        
    }
//console.log(markets[test.markets[test.market].name].precision,markets[test.markets[test.market].name].limits )
    //apply precision
    let dec_qty = new Decimal(qty)
    let final_qty = ''
    
    if(side === 'buy'){
        final_qty = dec_qty.toFixed(markets[test.markets[test.index].market.name].precision.quote, Decimal.ROUND_DOWN)
    }else{
        final_qty = dec_qty.toFixed(markets[test.markets[test.index].market.name].precision.base, Decimal.ROUND_DOWN)
    }
    
    let dec_price = new Decimal(price)
    let final_price = ''
    final_price = dec_price.toFixed(markets[test.markets[test.index].market.name].precision.price, Decimal.ROUND_DOWN)

    return {qty: parseFloat(final_qty), price: parseFloat(final_price), side: side}
}

export async function setPrice(test: complete_test_info, side: string){
    
    let quote_balance = 0
    let base_balance = 0
    if(test.type === 'real'){
        // query binance balance
        // get account info
        let account = await db_account.getOne(test.original.user_id, test.original.account_id)
        // create new exhcange class
        await updateAllBalances(account)

        quote_balance = (await db_real_balance.getOne(test.original.account_id, test.markets[test.index].market.name.split("/")[0])).free
        base_balance = (await db_real_balance.getOne(test.original.account_id, test.markets[test.index].market.name.split("/")[1])).free
    }else{
        quote_balance = (await db_test_balance.getOne(test.original.id, test.markets[test.index].market.name.split("/")[0])).free
        base_balance = (await db_test_balance.getOne(test.original.id, test.markets[test.index].market.name.split("/")[1])).free
    }

    let qty = 0
    // use base_amount_override or algo amount value
    //qty should be in quote currency
    switch (side) {
        case 'buy':
                qty = base_balance / test.markets[test.index].trade_info.last.price
            break;
        case 'sell':
                qty = quote_balance
            break;
    }

    let result = await applyFilters(test, qty, test.markets[test.index].trade_info.last.price, side);

    if(test.type !== 'real' && result.qty > 0 && result.price > 0){
        // update balances qty
        switch (side) {
            case 'buy':
                // quote_balance + (qty/price)
                // base_balance - qty
                // emulates commisison
/*     if(test.type !== 'real'){
        qty = qty * 0.99925 
    } */
                quote_balance = quote_balance + result.qty
                base_balance = (base_balance - ((result.qty * result.price)*1.00075)) > 0 ? (base_balance - ((result.qty * result.price)*1.00075)) : 0
                break;
        
            case 'sell':
                // base_balance + (qty*price)
                // quote_balance - qty
                base_balance = base_balance + (result.qty * result.price)
                quote_balance = (quote_balance - (result.qty*1.00075)) > 0 ? (quote_balance - (result.qty*1.00075)) : 0
                break;
        } 
        //console.log('setting quote balance', quote_balance)
        let new_balances = [{
                            test_id: test.original.id,
                            symbol: test.markets[test.index].market.name.split("/")[0],
                            free: quote_balance,
                            locked: 0    
                            },{
                            test_id: test.original.id,
                            symbol: test.markets[test.index].market.name.split("/")[1],
                            free: base_balance,
                            locked: 0   
                            }]
        await db_test_balance.upsert(new_balances[0])
        await db_test_balance.upsert(new_balances[1])
    }else if(test.type !== 'real' && result.qty === -1){
        // amount is too low, setting balance to 0
        switch (side) {
            case 'buy':
                // quote_balance + (qty/price)
                // base_balance - qty
                //quote_balance = quote_balance
                base_balance = 0
                break;
        
            case 'sell':
                // base_balance + (qty*price)
                // quote_balance - qty
                //base_balance = base_balance
                quote_balance = 0
                break;
        } 
        let new_balances = [{
                    test_id: test.original.id,
                    symbol: test.markets[test.index].market.name.split("/")[0],
                    free: quote_balance,
                    locked: 0    
                    },{
                    test_id: test.original.id,
                    symbol: test.markets[test.index].market.name.split("/")[1],
                    free: base_balance,
                    locked: 0   
                    }]
        await db_test_balance.upsert(new_balances[0])
        await db_test_balance.upsert(new_balances[1])
        result.qty = 0
    }else if(result.qty === -1){
        result.qty = 0
    }

    [test.markets[test.index].trade_info.last.qty, test.markets[test.index].trade_info.last.price] = [result.qty, result.price]

    return test.markets[test.index].trade_info
}

export async function checkBalance(test: complete_test_info, side: string){
    let balance = 0
    if(test.type === 'real'){
        if(side === 'buy'){
            balance = (await db_real_balance.getOne(test.original.account_id, test.markets[test.index].market.name.split("/")[1])).free
        }else{
            balance = (await db_real_balance.getOne(test.original.account_id, test.markets[test.index].market.name.split("/")[0])).free
        }
    }else{
        if(side === 'buy'){
            balance = (await db_test_balance.getOne(test.original.id, test.markets[test.index].market.name.split("/")[1])).free
        }else{
            balance = (await db_test_balance.getOne(test.original.id, test.markets[test.index].market.name.split("/")[0])).free
        }
    }

    let flag = false

    if(balance > 0 ){
        flag = true
    }
    return { balance: balance, result: flag }
}