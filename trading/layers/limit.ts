import { write } from '../tools/logs';
import { recordFakeTrade } from '../tools/trades';
import { setPrice } from '../tools/calculations'
import { complete_test_info } from '../../interfaces/index';
import * as db_account from '../../db/account'
import { postLimitOrder } from '../../binance';
import { activate } from '../tools/stall';
const debug = false

export async function LimitBuyLayer(test: complete_test_info, trigger: boolean){
    // buy at a specific price+% defined in the algo 
    // pref.post_price : 5 == 5% higher
    // will follow candles if price is not met

    if(test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].percent !== 0 && test.type !== 'real'){
        let candle = test.markets[test.index].market_data[test.candle.i]
        if(test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price === 0){
            // not limit price specified, this is a first candle
            if(trigger && candle[3] <= (candle[1] * (1 - (test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].percent / 100)))){
                // candle low is lower than proposed limit price AND testTrigger is true, order passed in the first candle.
                test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price = (candle[1] * (1 - (test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].percent / 100)))
                test.markets[test.index].trade_info.last.price = test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price
                // setting trading price as proposed limit price
                test.markets[test.index].trade_info = await setPrice(test, 'buy')
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: buy ${test.markets[test.index].trade_info.last.qty} at ${test.markets[test.index].trade_info.last.price} open was ${candle[1]} low was ${candle[3]}`
                )
            }else if(trigger){
                // test trigger is true BUT not enough room in candle, will wait for a lower price.
                test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price =  candle[1] * (1 - (test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].percent / 100))
                // setting limit price.
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: buy not enough room in candle, will wait for price at ${test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price.toFixed(8)} or lower`
                )
                test = await activate(test, 'buy') // activating stall protection
            }
        }else{
            // limit price is already set
            // currently tracking a price from not enough room
            
            if((candle[3] <= test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price) && test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price > 0){
                // if candle low <= limit price AND price > 0
                test.markets[test.index].trade_info.last.price = test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price
                test.markets[test.index].trade_info = await setPrice(test, 'buy')
                // lowest is higher than limit price, order passed
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: buy candle low was lower than saved price: ${test.markets[test.index].trade_info.last.price.toFixed(8)}`
                )
                //test = await reset(test)
            }else{
                // still not enough room, will wait again.
                // does not respect stall protect ?
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: buy candle low was higher than saved price, waiting: ${test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price.toFixed(8)}`
                )
                test.markets[test.index].trade_info.last.price = 0
            }
        }
    }else if(test.type === 'real' && trigger){
        let side = 'lower'
        test.markets[test.index].trade_info.last.price = test.markets[test.index].market_data[test.candle.i][1] * (1 - (test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].percent / 100))
        /* post limit trade */
        test.markets[test.index].trade_info = await setPrice(test, 'buy')
        // get account info
        if(test.markets[test.index].trade_info.last.qty !== 0 && test.markets[test.index].trade_info.last.price !== 0){
            write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                'limit',
                test.original.id,
                debug,
                `: buy posting limit order ${test.markets[test.index].trade_info.last.qty} for price ${test.markets[test.index].trade_info.last.price}, ${test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].percent}% ${side}`
            )

            let account = await db_account.getOne(test.original.user_id, test.original.account_id)
            // create new exhcange class
            await postLimitOrder(account, test.markets[test.index].market, 'buy', test.markets[test.index].trade_info.last.qty, test.markets[test.index].trade_info.last.price)
        }
        test = await activate(test, 'buy')
    }else{
        test.markets[test.index].trade_info.last.price = 0
    }

    if(test.markets[test.index].trade_info.last.price != 0){
        test = await recordFakeTrade(test, 'buy');
    }
    return test
}

export async function LimitSellLayer(test: complete_test_info, trigger: boolean){
    // trade at a specific price+% defined in the algo 
    // pref.post_price : 5 == 5% higher
    // will follow candles if price is not met

    if(test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].percent !== 0 && test.type !== 'real'){
        let candle = test.markets[test.index].market_data[test.candle.i]
        if(test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price === 0){
            // not limit price specified, this is a first candle
            if(trigger && candle[2] >= (candle[1] * (1 + (test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].percent / 100)))){
                // high is higher than proposed limti price AND testTrigger is true.
                // immediate sell
                test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price = (candle[1] * (1 + (test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].percent / 100)))
                test.markets[test.index].trade_info.last.price = test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price
                test.markets[test.index].trade_info = await setPrice(test, 'sell')
                // setting trading price as proposed limit price
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: sell ${test.markets[test.index].trade_info.last.qty * test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price} at ${test.markets[test.index].trade_info.last.price} open was ${candle[1]} low was ${candle[3]}`
                )
            }else if(trigger){
                // test trigger is true BUT not enough room in candle, will wait for a higher price.
                test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price = candle[1] * (1 + (test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].percent / 100))
                write(candle[0],
                test.markets[test.index].market.name,
                'limit',
                test.original.id,
                debug,
                `: sell not enough room in candle, will wait for price at ${test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price.toFixed(8)} or lower`
                )
                test = await activate(test, 'sell') // activating stall protection
                
            }
        }else{
            // limit price already set
            // currently tracking a price from not enough room

            if((candle[2] >= test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price) && test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price > 0){ 
                // if high >= limit price AND price > 0
                test.markets[test.index].trade_info.last.price = test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price
                test.markets[test.index].trade_info = await setPrice(test, 'sell')
                // lowest is higher than limit price, order passed
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: sell candle high was higher than saved price: ${test.markets[test.index].trade_info.last.price.toFixed(8)}`
                )
                //test = await reset(test)
            }else if(test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price > 0){
                // still not enough room, will wait again.
                write(candle[0],
                    test.markets[test.index].market.name,
                    'limit',
                    test.original.id,
                    debug,
                    `: sell candle high was lower than saved price, waiting: ${test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price.toFixed(8)}`
                )
                test.markets[test.index].trade_info.last.price = 0
            }
        }
    }else if(test.type === 'real' && trigger){
        let side = 'lower'
        test.markets[test.index].trade_info.last.price = test.markets[test.index].market_data[test.candle.i][1] * (1 + (test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].percent / 100))
        side = 'higher'
        /* post limit trade */
        test.markets[test.index].trade_info = await setPrice(test, 'sell')
        // get account info
        if(test.markets[test.index].trade_info.last.qty !== 0 && test.markets[test.index].trade_info.last.price !== 0){
            write(test.markets[test.index].market_data[test.candle.i][0],
                test.markets[test.index].market.name,
                'limit',
                test.original.id,
                debug,
                `: sell posting limit order ${test.markets[test.index].trade_info.last.qty} for price ${test.markets[test.index].trade_info.last.price}, ${test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].percent}% ${side}`
            )

            let account = await db_account.getOne(test.original.user_id, test.original.account_id)
            // create new exhcange class
            await postLimitOrder(account, test.markets[test.index].market, 'sell', test.markets[test.index].trade_info.last.qty, test.markets[test.index].trade_info.last.price)
        }
        test = await activate(test, 'sell')
    }else{
        test.markets[test.index].trade_info.last.price = 0
    }

    if(test.markets[test.index].trade_info.last.price != 0){
       test = await recordFakeTrade(test, 'sell');
    }
    return test
}