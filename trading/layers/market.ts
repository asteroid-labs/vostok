import { recordFakeTrade } from '../tools/trades';
import { setPrice } from '../tools/calculations'
import { complete_test_info } from '../../interfaces/index';
import * as db_account from '../../db/account'
import { postMarketOrder } from '../../binance';

const debug = false

// market buy layer
export async function MarketBuyLayer(test: complete_test_info, trigger: boolean){

    if(trigger){
        test.markets[test.index].trade_info.last.price = test.markets[test.index].market_data[test.candle.i][1]
        test.markets[test.index].trade_info = await setPrice(test, 'buy')
    } else{
        test.markets[test.index].trade_info.last.price = 0    
    }  

    if(test.markets[test.index].trade_info.last.price !== 0 && test.markets[test.index].trade_info.last.qty !== 0){
        if(test.type === 'real'){
            /* post market trade */

            // get account info
            let account = await db_account.getOne(test.original.user_id, test.original.account_id)
            // create new exhcange class
            await postMarketOrder(account, test.markets[test.index].market, 'buy', test.markets[test.index].trade_info.last.qty)
        }
        test = await recordFakeTrade(test, 'buy');
        test.markets[test.index].trade_info.last.price = 0
    }
    return test
}

// market sell layer
export async function MarketSellLayer(test: complete_test_info, trigger: boolean){

    if(trigger){
        test.markets[test.index].trade_info.last.price = test.markets[test.index].market_data[test.candle.i][1]
        test.markets[test.index].trade_info = await setPrice(test, 'sell')
    } else{
        test.markets[test.index].trade_info.last.price = 0    
    }  

    if(test.markets[test.index].trade_info.last.price !== 0 && test.markets[test.index].trade_info.last.qty !== 0){
        if(test.type === 'real'){
            /* post market trade */

            // get account info
            let account = await db_account.getOne(test.original.user_id, test.original.account_id)
            // create new exhcange class
            await postMarketOrder(account, test.markets[test.index].market, 'sell', test.markets[test.index].trade_info.last.qty)
        }
        test = await recordFakeTrade(test, 'sell');
        test.markets[test.index].trade_info.last.price = 0
    }
    return test
}