import { write } from '../tools/logs';
import { complete_test_info } from '../../interfaces/index';
import { loopBuyLayers, loopSellLayers } from '../layers/loop';
import { verifyStopLoss } from '../tools/stoploss';
const debug = false

/* need to rework this */
export async function TrailBuyStrategy(test: complete_test_info, trigger: boolean){
    // will trail the price drops using defined % in algo
    let traded = false
    let trailing = false

    let candle = test.markets[test.index].market_data[test.candle.i]
    let init = false
    if(trigger && test.markets[test.index].trade_info.trail_price === 0){
        // first candle, need to active trail and set price
        test.markets[test.index].trade_info.trail_price = candle[1]
        init = true
        test.algo.buy_pref.trail.active = true
        test.algo.buy_pref.trail.trigger = test.algo.buy_pref.trail_trig
        test.algo.buy_pref.trail.stop = test.algo.buy_pref.trail_stop
        write(candle[0],
            test.markets[test.index].market.name,
            'trailing',
            test.original.id,
            debug,
            `: buy trailing activated, next step trigger is -${test.algo.buy_pref.trail.trigger.toFixed(3)}% / ${(test.markets[test.index].trade_info.trail_price * (1 - (test.algo.buy_pref.trail.trigger / 100))).toFixed(7)}, stop is ${test.algo.buy_pref.trail.stop.toFixed(3)}% / ${(test.markets[test.index].trade_info.trail_price * (1 - (test.algo.buy_pref.trail.trigger / 100))* (1 + (test.algo.buy_pref.trail.stop / 100))).toFixed(7)}`
        ) 
    }

    if(test.markets[test.index].trade_info.trail_price !== 0 && !init){
        // second candle after activation
        let next_step = 0
        let next_stop = 0

        next_step = test.markets[test.index].trade_info.trail_price * (1 - (test.algo.buy_pref.trail.trigger / 100))
        next_stop = next_step * (1 + (test.algo.buy_pref.trail.stop / 100))
        //console.log(candle[1], next_step, next_stop)
        if(candle[1] <= next_step){
            trailing = true
            test.algo.buy_pref.trail.trigger = ((test.markets[test.index].trade_info.trail_price - candle[1]) / test.markets[test.index].trade_info.trail_price) * 100 
            test.markets[test.index].trade_info.price = test.markets[test.index].trade_info.trail_price * (1 - (test.algo.buy_pref.trail.trigger / 100))
            //console.log(1,candle[1], next_step)
        }
        if(candle[1] >= next_stop){
            test.markets[test.index].trade_info.trail_price = 0
            test.markets[test.index].trade_info.price = next_stop
            traded = true
            //console.log(2,candle[1], next_stop)
        }
    }

    if(trailing){
        // low is lower than open*post_price AND testTrigger is true.
        write(candle[0],
            test.markets[test.index].market.name,
            'trailing',
            test.original.id,
            debug,
            `: buy new trigger set at -${test.algo.buy_pref.trail.trigger.toFixed(3)}% / ${test.markets[test.index].trade_info.price.toFixed(7)}`
        )
    }
    if(traded){
        write(candle[0],
            test.markets[test.index].market.name,
            'trailing',
            test.original.id,
            debug,
            `: buy trailing stop hit at ${(test.algo.buy_pref.trail.stop - test.algo.buy_pref.trail.trigger).toFixed(3)}% / ${test.markets[test.index].trade_info.price.toFixed(7)}, open is ${test.markets[test.index].market_data[test.candle.i][1]}`
        )
        test.algo.buy_pref.trail.active = false
        test.algo.buy_pref.trail.trigger = test.algo.buy_pref.trail_trig
        test.algo.buy_pref.trail.stop = test.algo.sell_pref.trail_stop
        
        trigger = true
    }else{
        trigger = false
    }
    test = await loopBuyLayers(test, trigger)
    return test
}

export async function TrailSellStrategy(test: complete_test_info, trigger: boolean){
    // will trail the price rises using defined % in algo
    let traded = false
    let trailing = false
    test = await verifyStopLoss(test) // stoploss will sell if price below x% latest bought price

    let candle = test.markets[test.index].market_data[test.candle.i]
    let init = false
    if(trigger && test.markets[test.index].trade_info.trail_price === 0){
        // first candle, need to active trail and set price
        test.markets[test.index].trade_info.trail_price = candle[1]
        init = true
        test.algo.sell_pref.trail.active = true
        test.algo.sell_pref.trail.trigger = test.algo.sell_pref.trail_trig
        test.algo.sell_pref.trail.stop = test.algo.sell_pref.trail_stop
        write(candle[0],
            test.markets[test.index].market.name,
            'trailing',
            test.original.id,
            debug,
            `: sell trailing activated, next step trigger is ${test.algo.sell_pref.trail.trigger.toFixed(3)}% / ${(test.markets[test.index].trade_info.trail_price * (1 + (test.algo.sell_pref.trail.trigger / 100))).toFixed(7)}, stop is -${test.algo.sell_pref.trail.stop.toFixed(3)}% / ${(test.markets[test.index].trade_info.trail_price * (1 + (test.algo.sell_pref.trail.trigger / 100))* (1 - (test.algo.sell_pref.trail.stop / 100))).toFixed(7)}`
        )
    }

    if(test.markets[test.index].trade_info.trail_price !== 0 && !init){
        let next_step = 0
        let next_stop = 0

        next_step = test.markets[test.index].trade_info.trail_price * (1 + (test.algo.sell_pref.trail.trigger / 100))
        next_stop = next_step * (1 - (test.algo.sell_pref.trail.stop / 100))
        if(candle[1] >= next_step){
            trailing = true
            test.algo.sell_pref.trail.trigger = ((candle[1] - test.markets[test.index].trade_info.trail_price) / test.markets[test.index].trade_info.trail_price) * 100 
            test.markets[test.index].trade_info.price = test.markets[test.index].trade_info.trail_price * (1 + (test.algo.sell_pref.trail.trigger / 100))
            //console.log(3,candle[1], next_sell_step)
        }
        if(candle[1] <= next_stop){
            test.markets[test.index].trade_info.trail_price = 0
            test.markets[test.index].trade_info.price = next_stop
            traded = true
            //console.log(4,candle[1], next_sell_stop)
        }
    }
    if(trailing){
        // low is lower than open*post_price AND testTrigger is true.
        write(candle[0],
            test.markets[test.index].market.name,
            'trailing',
            test.original.id,
            debug,
            `: sell new trigger set at ${test.algo.sell_pref.trail.trigger.toFixed(3)}% / ${test.markets[test.index].trade_info.price.toFixed(7)}`
        )
    }
    if(traded){
        write(candle[0],
            test.markets[test.index].market.name,
            'trailing',
            test.original.id,
            debug,
            `: sell trailing stop hit at ${(test.algo.sell_pref.trail.trigger - test.algo.sell_pref.trail.stop).toFixed(3)}% / ${test.markets[test.index].trade_info.price.toFixed(7)}, open is ${test.markets[test.index].market_data[test.candle.i][1]}`
        )
        test.algo.sell_pref.trail.active = false
        test.algo.sell_pref.trail.trigger = test.algo.sell_pref.trail_trig
        test.algo.sell_pref.trail.stop = test.algo.sell_pref.trail_stop

        trigger = true
    }else{
        trigger = false
    }
    test = await loopSellLayers(test, trigger)
    return test
}