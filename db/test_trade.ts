const debug = false
import { pool } from './index';
import { test_trade }from '../interfaces/index';

export async function create(trade: test_trade) {
    let res = {} as any
    let text = 'INSERT INTO test_trades (test, time, pair_id, qty, price, side, account_base_value, account_quote_value) '+
                'VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ON CONFLICT DO NOTHING RETURNING id;'
    let values = [
        trade.test,
        trade.time,
        trade.pair_id,
        trade.qty,
        trade.price,
        trade.side,
        trade.account_base_value,
        trade.account_quote_value
    ]
    try {
        res = await pool.query(text, values) // upsert
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done save test_trade', trade)
        }
    } catch (err) {
        console.log(err.stack, 'error create test_trade', trade)
    }
    return res.rows[0] as test_trade
}

export async function get(id = 0) {
    let res = {} as any
    if(id === 0){
        try {
            res = await pool.query(`SELECT * FROM test_trades;`)
        } catch (err) {
            console.log(err.stack, 'error get test_trade all')
        }
    }else{
        try {
            res = await pool.query('SELECT * FROM test_trades WHERE id = $1 ORDER BY id ASC;', [id])
        } catch (err) {
            console.log(err.stack, 'error get test_trade')
        }
        res.rows = res.rows[0] as test_trade
    }
    return res.rows as test_trade[]
}

export async function getLast(id: number, side = '') {
    let res = {rows: []} as any
    if(typeof id !== 'undefined' && side === ''){
        try {
            res = await pool.query('SELECT * FROM test_trades WHERE test = $1 ORDER BY time DESC limit 1;', [id])
        } catch (err) {
            console.log(err.stack, 'error getLast test_trade')
        }
    }else if(typeof id !== 'undefined'){
        try {
            res = await pool.query('SELECT * FROM test_trades WHERE test = $1 AND side = $2 ORDER BY time DESC limit 1;', [id, side])
        } catch (err) {
            console.log(err.stack, 'error getLast test_trade side', id, side)
        }
    }else{
        res.rows.push({test:0, time:0, qty:0, price:0, side: side})
    }

    return res.rows[0] as test_trade
}

export async function getChilds(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM test_trades WHERE test = $1 ORDER BY id ASC;', [id])
        debug ? console.log('testChilds trades: ', res.rows) :false
    } catch (err) {
        console.log(err.stack, 'error getChilds test_trade')
    }
    return res.rows as test_trade[]
}

export async function getTradeGains(test: number, trade:number){
    //console.log(test, trade)
    
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM test_trades WHERE test = $1 AND id <= $2 ORDER BY id ASC;', [test, trade])
    } catch (err) {
        console.log(err.stack, 'error getTradeGains last')
    }
    // get a sum of test's firsts buy trades
    let last_trades_first = []
    for (let index = 0; index < res.rows.length; index++) {
        if(res.rows[index].side === res.rows[0].side){
            last_trades_first.push(res.rows[index].qty * res.rows[index].price)
        }else{
            //loop is done
            index = res.rows.length
        }
    }

    // get a sum of last trades of same type
    res.rows = res.rows.reverse()
    //console.log(res.rows)
    let last_trades = []
    for (let index = 0; index < res.rows.length; index++) {
        if(res.rows[index].side === res.rows[0].side){
            last_trades.push(res.rows[index].qty * res.rows[index].price)
        }else{
            //loop is done
            index = res.rows.length
        }
    }
    let last_trades_sum = last_trades.length > 1 ? last_trades.reduce((a,b) => (a+b)) : last_trades[0]

    let last_trades_sum_first = last_trades_first.length > 1 ? last_trades_first.reduce((a,b) => (a+b)) : last_trades_first[0]

    let gain = last_trades_sum - last_trades_sum_first

    let percent = gain / last_trades_sum_first * 100

    return { gain: gain, percent: percent }
}

export async function unlinkChilds(id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM test_trades WHERE test = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error unlinkChilds test_trade')
    }
    return true
}

export async function unlink(id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM test_trades WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error unlink test_trade')
    }
    return true
}

export async function unlinkByMarket(id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM test_trades WHERE pair_id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error unlink by market test_trade')
    }
    return true
}