import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.qstick);
    const close: number[] = data.map(candle => candle[4]);
    const open: number[] = data.map(candle => candle[1]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.qstick.indicator([open, close], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate qstick indicator: ${err}`);
    }
}