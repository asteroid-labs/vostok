import * as db_user from '../../../db/user'
import * as db_account from '../../../db/account'
import * as db_algo from '../../../db/algo'
import * as db_timeframe from '../../../db/timeframe'
import * as db_exchange from '../../../db/exchange'
import * as db_test from '../../../db/tests'
import * as db_test_balance from '../../../db/test_balance'
import * as test_algo from '../../../db/tests/functions/createTestAlgo'
import { test, account, user } from 'interfaces/index';
import * as db_test_trade from '../../../db/test_trade';
import * as db_test_logs from '../../../db/test_logs';

/* expected trades */
import * as market_trades from './market_trades.json'
import * as market_trades_stop_loss from './market_trades_stop_loss.json'
import * as market_trades_stop_loss_limit from './market_trades_stop_loss_limit.json'
import * as limit_trades from './limit_trades.json'
import * as multi_market_trades from './multi_market_trades.json'
import * as limit_trades_multi from './limit_trades_multi.json'
import * as trail_trades from './trail_trades.json'

import * as backtesting from '../index';

/* global vars */
var indicator_id = 0
var user_id = 0
var user_email = ''
var account_id = 0
var algo_id = 0
var timeframe_id = 0
var market_id = 0
var market_ids = [0]
var exchange_id = 0
var backtest_id = [0]

beforeAll(() => {
  return initializeBacktest();
});

afterAll(() => {
  return clearBacktests();
});

async function initializeBacktest(){
  let exchange = await db_exchange.createFirstExchange('testing')
  exchange_id = exchange.id

  let user = {
    "id": 42,
    "name": 'Testing',
    "email": 'test@asteroidlabs.io',
    "password": 'testing_password',
    "prefs": JSON.stringify('{Test: Testing}')
  } as user   
  let usr = await db_user.create(user)
  // Created OK

  usr = await db_user.find('test@asteroidlabs.io')
  user.id = usr.id
  usr = await db_user.update(user)
  // Find OK
  user_id = user.id
  user_email = user.email

  let account = {
      "id": 42,
      "name": "Testing Account",
      "user_id" : user_id,
      "exchange" : exchange_id,
      "api_key": "API_KEY",
      "api_secret": "API_SECRET",
      "prefs": JSON.stringify({telegram_notifications: "off", email_notifications: "off"}),
      "trading_status": "test"
    } as account
    
  let result = await db_account.create(account)
  // Find OK
  account_id = result
  // create
  expect(result).toBeTruthy()
  // user and accounts were created ok

  let algo_result = await test_algo.createTestAlgo(user_id, account_id)
  // create test algo
  expect(algo_result.algo).toBeTruthy()
  /* save to globals */
  indicator_id = algo_result.indicator as number
  algo_id = algo_result.algo
  timeframe_id = algo_result.timeframe
  market_id = algo_result.market
  market_ids = algo_result.markets
}

async function clearBacktests(){
  for(const backtest of backtest_id){
    await db_test_logs.unlinkAll(backtest)
    await db_test_trade.unlink(backtest)
    //expect(unlink_al).toBeTruthy()
    await db_test_balance.unlinkAll(backtest)
    await db_test.unlink(backtest)
  }
  await db_algo.unlink(user_id, algo_id)
  //expect(unlink_al).toBeTruthy()
  await db_timeframe.unlink(timeframe_id)
  //expect(unlink_t).toBeTruthy()
  //await db_balance.unlinkAll(account_id)
  await db_account.unlink(user_id, account_id)
  //expect(unlink_a).toBeTruthy()
  await db_user.unlink(user_id, user_email)
  //expect(unlink_u).toBeTruthy()
}


test('Backtest - Simple Single Market Orders', async()=>{
  jest.setTimeout(15 * 60000);

  let result = await test_algo.updateMarketAlgo(user_id, account_id, algo_id, timeframe_id, indicator_id)
  expect(result.algo_result).toBeTruthy()

  let algo = await db_algo.getOne(user_id, account_id, result.algo)
  expect(algo.id).toEqual(result.algo)
  //algo Market is OK
  await testBacktest(result, market_trades.trades)
  // market backtest are ok
})

test('Backtest - Simple Single Limit Orders', async()=>{
  jest.setTimeout(10 * 60000);
  let result = await test_algo.updateLimitAlgo(user_id, account_id, algo_id, timeframe_id, indicator_id)

  await testBacktest(result, limit_trades.trades)
  //algo Limit is OK 
})

test('Backtest - Multi-Markets Single Limit Orders', async()=>{
  jest.setTimeout(25 * 60000);
  let result = await test_algo.updateMarketAlgo(user_id, account_id, algo_id, timeframe_id, indicator_id)

  await testBacktest(result, multi_market_trades.trades, 'multi')
  //algo Limit is OK
})

test('Backtest - Simple Multiple Layered Market and Limit Orders', async()=>{
  jest.setTimeout(10 * 60000);
  let result = await test_algo.updateMarketAlgoMutliLayers(user_id, account_id, algo_id, timeframe_id, indicator_id)

  await testBacktest(result, limit_trades_multi.trades)
  //algo Limit is OK
})

test('Backtest - Trailing Single Buy Market, Sell Limit Orders', async()=>{
  jest.setTimeout(10 * 60000);
  let result = await test_algo.updateTrailAlgo(user_id, account_id, algo_id, timeframe_id, indicator_id)

  await testBacktest(result, trail_trades.trades)
  //algo Trail is OK
})

// add one for stop-loss
test('Backtest - Simple Single Market Orders With Sell Stop-Loss', async()=>{
  jest.setTimeout(10 * 60000);
  let result = await test_algo.updateMarketStopLossAlgo(user_id, account_id, algo_id, timeframe_id, indicator_id)

  await testBacktest(result, market_trades_stop_loss.trades)
})

test('Backtest - Simple Single Market Orders With Sell Limit Stop-Loss', async()=>{
  jest.setTimeout(12 * 60000);
  let result = await test_algo.updateMarketStopLossLimitAlgo(user_id, account_id, algo_id, timeframe_id, indicator_id)

  await testBacktest(result, market_trades_stop_loss_limit.trades)
})
// add one for multi-market

async function testBacktest(result: any, expected_trades: any, type = ''){

  let backtest = {
    time: 1576866231465,
    type: 'backtest',
    user_id: user_id,
    algo_id: algo_id,
    exchange_id: exchange_id,
    pair_ids: type == 'multi' ? JSON.stringify(market_ids) : JSON.stringify(market_id),
    account_id: account_id,
    base_amount: 0.5,
    start: 1575176400000,
    stop: 1579551329000,
    state: 'init'
  } as test

  let backtest_result = await db_test.create(backtest)
  backtest.id = backtest_result.id
  backtest_id.push(backtest_result.id)
  let expected_backtest = {
    id: backtest.id,
    time: "1576866231465",
    type: 'backtest',
    user_id: user_id,
    algo_id: algo_id,
    exchange_id: exchange_id,
    pair_ids: type == 'multi' ? market_ids : market_id,
    account_id: account_id,
    base_amount: 0.5,
    start: "1575176400000",
    stop: "1579551329000",
    state: 'init'
  }

  backtest_result = await db_test.get(backtest.id)

  expect(backtest_result).toEqual(expected_backtest)
  //backtest is created and equal

  await backtesting.test(backtest.id)

  let trades = await db_test_trade.getChilds(backtest.id)

  console.log(JSON.stringify(trades))

  for (const [i, trade] of trades.entries()){
    expected_trades[i].id = trade.id as number 
    expected_trades[i].pair_id = trade.pair_id
    expected_trades[i].test = trade.test 
  }
  console.log(JSON.stringify(trades))

  expect(trades).toEqual(expected_trades)
  // market trades are as expected
  return [result, expected_trades, trades]
}
