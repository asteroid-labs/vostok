import { Pool, Client } from 'pg'
import * as config from '../config.json';
const pool = new Pool({
    user: config.db.user,
    host: config.db.host,
    database: config.db.db,
    password: config.db.password,
    port: config.db.port,
    max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
})

export { pool };
const fs = require('fs').promises;

async function loadFile() {
    const data = await fs.readFile(__dirname+"/../../db/init/db.pgsql", "utf-8");
    return data;
}
export async function checkTables() {
    let res = [] as any
    try {
        res = await pool.query(`SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_name = 'users');`)
    } catch (err) {
        console.log(err.stack)
    }

    if (!res.rows[0].exists) {
        try{
            let init = await loadFile()
            res = await pool.query(init)
        }catch(err){
            console.log(err)
        }
        
    }
}