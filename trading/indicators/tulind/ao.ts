import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.ao);
    const high: number[] = data.map(candle => candle[2]);
    const low: number[] = data.map(candle => candle[3]); // [4,5,6,6,6,5,5,5,6,4]
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.ao.indicator([high, low], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate ao indicator: ${err}`);
    }
}