import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.adxr);
    const high: number[] = data.map(candle => candle[2]);
    const low: number[] = data.map(candle => candle[3]);
    const close: number[] = data.map(candle => candle[4]); // [4,5,6,6,6,5,5,5,6,4]
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.adxr.indicator([high, low, close], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate adxr indicator: ${err}`);
    }
}