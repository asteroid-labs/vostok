import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    const close: number[] = data.map(candle => candle[4]); // [4,5,6,6,6,5,5,5,6,4]
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.rsi.indicator([close], [period]);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate RSI indicator: ${err}`);
    }
}