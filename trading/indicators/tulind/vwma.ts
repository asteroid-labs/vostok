import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.vwma);
    const close: number[] = data.map(candle => candle[4]);
    const volume: number[] = data.map(candle => candle[5]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.vwma.indicator([close, volume], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate vwma indicator: ${err}`);
    }
}