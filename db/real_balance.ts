import { pool } from './index';
import { real_balance } from '../interfaces/index';
const debug = false

export async function upsert(balance: real_balance) {
    let res = {} as any
    let text = 'INSERT INTO real_balances (account_id, symbol, free, locked) '+
                'VALUES ($1, $2, $3, $4) ON CONFLICT ON CONSTRAINT unique_real_balances_con '+
                'DO UPDATE SET (free, locked) = ($3, $4) RETURNING account_id, symbol, free, locked;'
    let values = [
        balance.account_id,
        balance.symbol,
        balance.free,
        balance.locked
    ]
    try {
        res = await pool.query(text, values) // upsert
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done save real_balance')
        }
    } catch (err) {
        console.log(err.stack, 'error upsert real_balance', balance)
    }
    return res.rows[0] as real_balance
}

export async function getOne(account_id: number, symbol: string) {
    let res = {} as any
    let text = 'SELECT * FROM real_balances WHERE account_id = $1 AND symbol = $2;'
    let values = [account_id, symbol]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error get real_balances')
    }
    let balance = {
            account_id: account_id,
            symbol: symbol,
            free:0,
            locked:0
        } as real_balance
    if(res.rows.length === 0){
        balance = await upsert(balance)
        console.log('no real_balance exists', res.rows, balance)
    }else{
        balance = res.rows[0]
    }
    return balance
}

export async function getAll(account_id: number, notNull = false) {
    let res = {} as any
    let text = 'SELECT * FROM real_balances WHERE account_id = $1 AND (free > 0 OR locked > 0) ORDER BY symbol ASC;'
    if(notNull){
        try {
            res = await pool.query(text, [account_id])
        } catch (err) {
            console.log(err.stack, 'error getAll real_balances')
        }
    }else{
        try {
            res = await pool.query(`SELECT * FROM balances \
                    WHERE account_id = ${account_id} ORDER BY symbol ASC;`)
        } catch (err) {
            console.log(err.stack, 'error getAll real_balances')
        }
    }
    return res.rows as real_balance[]
}