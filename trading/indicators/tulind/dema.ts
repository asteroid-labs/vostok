import {indicators} from 'tulind';
  
export default async (data: number[][], period: number[]) => {
    // Raw candle dataset from exchange
    // Extract closing price values
    //console.log(indicators.dema);
    const open: number[] = data.map(candle => candle[1]);
    let results: number[][] = []
    try {
        let results: number[][] = await indicators.dema.indicator([open], period);
        return results[0]
    } catch (err) {
        throw new Error(`Failed to evaluate dema indicator: ${err}`);
    }
}