const debug = false
import { pool } from './index';
import { timeframe } from '../interfaces/index';

export async function createBaseTimeframes() {

    let data = [
        {'name': '1m', 'msec_value': 60000},
        {'name': '5m', 'msec_value': 300000},
        {'name': '15m', 'msec_value': 900000},
        {'name': '30m', 'msec_value': 1800000},
        {'name': '1h', 'msec_value': 3600000},
        {'name': '2h', 'msec_value': 7200000},
        {'name': '4h', 'msec_value': 14400000},
        {'name': '6h', 'msec_value': 21600000},
        {'name': '12h', 'msec_value': 43200000},
        {'name': '1D', 'msec_value': 86400000},
        {'name': '1W', 'msec_value': 604800000},
        {'name': '1M', 'msec_value': 2628002880},
    ]

    let existing = await getAll()
    if(existing.length === 0){
        for(const item of data){
            await create(item as timeframe)
        }
    }
}

export async function create(timeframe: timeframe) {
    let res = {} as any
    let text = 'INSERT INTO timeframes (name, msec_value) VALUES ($1, $2) ON CONFLICT DO NOTHING RETURNING ID;'
    let values = [timeframe.name, timeframe.msec_value]
        try {
            res = await pool.query(text, values) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save timeframe')
            }
        } catch (err) {
            console.log(err.stack, 'error timeframe')
        }
    return res.rows[0] as timeframe
}
export async function update(timeframe: timeframe) {
    let res = {} as any
    let text = 'UPDATE timeframes SET name = $1, msec_value = $2 WHERE id = $3;'
    let values = [timeframe.name, timeframe.msec_value, timeframe.id]
    try {
        res = await pool.query(text, values)
    } catch (err) {
        console.log(err.stack, 'error update timeframe')
    }
    return true
}

export async function unlink(id: number) {
    let res = {} as any
    try {
        res = await pool.query('DELETE FROM timeframes WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error delete timeframe')
    }
    return true
}

export async function getOne(id: number) {
    let res = {} as any
    try {
        res = await pool.query('SELECT * FROM timeframes WHERE id = $1;', [id])
    } catch (err) {
        console.log(err.stack, 'error get timeframe')
    }
    return res.rows[0] as timeframe

}

export async function getAll() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM timeframes ORDER BY msec_value ASC;`)
    } catch (err) {
        console.log(err.stack, 'error get timeframe')
    }
    return res.rows as timeframe[]
}