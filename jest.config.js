module.exports = {
    "roots": [
      "./trading",
      "./db"
    ],
    testMatch: [
      "**/__tests__/**/*.+(ts|tsx|js)",
      "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    "transform": {
      "^.+\\.(ts|tsx)$": "ts-jest"
    },
    globals: {
      'ts-jest': {
        packageJson: 'package.json',
      },
    },
  }