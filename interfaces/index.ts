
export interface user {
    id: number,
    name: string,
    email: string,
    password?: string,
    telegram_id?:string,
    prefs: string,
    accounts?: number[]
}

export interface account {
    id: number,
    user_id: number,
    name: string,
    exchange: number,
    exchanges: object[],
    api_key: string,
    api_secret: string,
    prefs: string,
    email_notifications: string,
    telegram_notifications: string,
    trading_status: string,
    balances?: real_balance[]
}
export interface algo {
    id: number,
    user_id: number,
    account_id: number,
    accounts?: account[],
    name: string,
    timeframes?: timeframe[]
    markets_min_volume: number,
    indicators?: indicator[],
    buy_pref: algo_pref,
    sell_pref: algo_pref,
    status: string,
    buy?: string,
    sell?: string,
    tests?: {backtest: test[], livetest: test[], realtest: test[]},
    profits?: number
}

export interface timeframe {
    id: number,
    name: string,
    msec_value: number | string
}

export interface indicator {
    id?: number,
    name: string,
    function: string,
    periods: number[]
}

export interface algo_indicator {
    id: number,
    name: string,
    trigger_cond: string, // '=', '>'
    trigger: number,
    fnc: Function,
    periods: number[],
    timeframe: indicator_timeframe
}

export interface test_trade {
    id?: number,
    pair_id: number,
    market?: market,
    test: number,
    time: number,
    qty: number,
    price: number,
    side: string,
    total?:number,
    account_base_value: number,
    account_quote_value: number,
    total_percent?:number
}

export interface test {
    id: number,
    type: string,
    time: number,
    exchange_id: number,
    pair_ids: string | number[],
    timeframe_id: number,
    user_id: number,
    account_id: number,
    algo_id: number,
    base_amount: number,
    state: string,
    start: number,
    stop: number,
    total?: number,
    total_percent?: number
    market_display?: string
    completion?: number 
}

export interface market {
    id: number,
    name: string,
    sputnik_id: number,
}
export interface Imarket {
    id: string,
    symbol: string,
    base: string,
    quote: string,
    baseId: string,
    quoteId: string,
    info: object,
    active: boolean,
    precision: { base: number, quote: number, amount: number, price: number },
    limits: {
        amount: { min: number, max: number },
        price: { min: number, max: number },
        cost: { min: number, max: number }
    }

}

export interface indicator_timeframe{
    id: number,
    msec_value: number,
    //market_data: number[][]
}

export interface algo_pref {
    indicators: algo_indicator[],
    layers: algo_layers[],
    layer_index: number,
    trail_trig: number,
    trail_gap_reduce: number,
    trail_stop: number,
    trail: trail,
    stop_loss?: number,
    stop_loss_limit?: number,
}

export interface market_candle{
    timed: number,
    opening_price: number,
    highest_price: number,
    lowest_price: number,
    closing_price: number,
    volume_base: number
}

export interface candle_info {
    //candle: number[],
    i: number,
}

export interface stall_protect {
    active: boolean, 
    current: number
}

export interface trail {
    active: boolean,
    trigger: number, 
    stop: number
}

export interface test_info {
    id: number,
    type: string,
    layer_index: number,
    buy: algo_pref,
    buy_stall_protect: stall_protect,
    buy_trail: trail,
    sell: algo_pref,
    sell_stall_protect: stall_protect,
    sell_trail: trail,
    user_id: number,
    account_id: number,
    last_trade_time: number
}
export interface trade_info {
    //state: string,
    id: number,
    last: {
        price: number, 
        qty: number
    },
    price: number,
    base_amount_override: number,
    trail_price: number
}

export interface market_datas {
    buy: number[][][],
    sell: number[][][]
}

export interface test_balance {
    test_id: number,
    symbol: string,
    free: number,
    locked: number,
}
export interface real_balance {
    account_id: number,
    symbol: string,
    free: number,
    locked: number,
    latest_btc_price?: number,
}

export interface test_log {
    time: number,
    test_time: number,
    id: number,
    text: string,
    type: string,
}

export interface market_test {
    market: market,
    market_data: number[][],
    last_trade_time: number,
    //trail: trail,
    trade_info: trade_info
}
export interface complete_test_info {
    original: test,
    index: number,
    markets: market_test[],
    algo: algo,
    type: string,
    candle: candle_info, //candle index and trigger result
}

export interface message_info {
    qty: number,
    base_value: number,
    quote: string,
    base: string,
    type: string,
    gain: number,
    gain_percent: number,
    test_id: number,
    side: string,
    price: number,
}

export interface execution_notif {
    eventType: string,
    eventTime: number,
    symbol: string,
    newClientOrderId: string,
    side: string,
    orderType: string,
    cancelType: string,
    quantity: string,
    price: string,
    stopPrice: string,
    icebergQuantity: string,
    g: number, // to be ignored
    originalClientOrderId: string,
    executionType: string,
    orderStatus:string,
    rejectReason: string,
    orderId: number,
    lastTradeQuantity: string,
    accumulatedQuantity: string,
    lastTradePrice: string,
    commission: string,
    commissionAsset: string,
    tradeTime: number,
    tradeId: number,
    I: number, // ignore
    w: boolean, // ignore
    maker: boolean
}

export interface binance_trade {
    info: {},
    timestamp: number,
    datetime: string,
    symbol: string,
    id: string,
    order: string,
    type: string,
    takerOrMaker: string,
    side: string,
    price: number,
    amount: number,
    cost: number,
    fee: { cost: number, currency: string }
}

export interface binance_order {
    "info": {},
    "id": string,
    "clientOrderId": string,
    "timestamp": number,
    "datetime": string,
    "symbol": string,
    "type": string,
    "side": string,
    "price": number,
    "amount": number,
    "cost": number,
    "average": number,
    "filled": number,
    "remaining": number,
    "status": string
}

export interface algo_layers{
    type: string,
    percent: number,
    qty: number,
    qty_percent: boolean,
    stall: number,
    stall_protect: stall_protect,
    price: number
}