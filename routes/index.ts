import passport from 'passport';
import LocalStrategy from 'passport-local';
import * as db from '../db/index';
import * as db_user from '../db/user';
import * as config from '../config.json'
import path from 'path';
import https from 'https';
import fs from 'fs';
import helmet from 'helmet';
import { Express, Request, Response, NextFunction } from 'express';
import session from 'express-session';
import bodyParser from 'body-parser';
import * as db_indicator from '../db/indicator';
import * as db_timeframe from '../db/timeframe';
import * as db_market from '../db/market';
import * as db_exchange from '../db/exchange';
import * as pkg from '../package.json';

// importing routes
import marketsRouter from './market';
import usersRouter from './user';
import accountsRouter from './account';
import algosRouter from './algo';
import timeframesRouter from './timeframe';
import indicatorsRouter from './indicator';

import testsRouter from './tests';
import { user } from '../interfaces/index';

const express = require('express');
const compression = require('compression')

const app: Express = express();

app.use(helmet())
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(session({
    secret: config.web.sessionSecret, // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.set('views', './src/views');
app.set('view engine', 'ejs');
app.locals.db = db
app.use('/static', express.static(path.join(__dirname, '../src/public')))
app.use(compression())

// passport.js things

passport.serializeUser(function(user, done) {
    return done(null, user); // need to work here
});

passport.deserializeUser(function(user, done) {
    return done(null, user); // need to work here
});
passport.use('local', new LocalStrategy.Strategy(
    async function(username, password, done) {
        let user = {} as any
        try {
            user = await db_user.find(username.toLowerCase())
        } catch (err) {
            console.log(err.stack)
            return done(err)
        }

        if (!user) {
            console.log('incorrect username.')
            return done(null, false, { message: 'Incorrect username.' });
        }

        if (!db_user.validPassword(password, user.password)) {
            console.log('incorrect password')
            return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);

    }
));


// express routes

app.get('/', async function(req, res) {
    // let marketsCount = await db.getMarketsCount('binance')
    // let dataCount = await db.getDataCount()
    let version = pkg.version
    // let data = { version: version, marketsCount: marketsCount.count, dataCount: dataCount.count }
    let data = { version: version }
    let admin = false
    const user = req.user as user
    if (user?.email.toLowerCase() === config.web.email) {
        admin = true
    }
    let auth = req.isAuthenticated()
    res.render('pages/index.ejs', {auth, admin, data });
});

app.get('/changelog', async function(req, res) {
    let version = pkg.version
    let data = { version: version }
    let admin = false

    let auth = false
    res.render('pages/changelog.ejs', {auth, admin, data });
});

app.get('/login', async function(req, res) {
    let auth = req.isAuthenticated()
    res.render('pages/login.ejs', {auth})
})

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

app.post('/dologin', passport.authenticate('local'),
    function(req, res) {
        //console.log(req, res)
    res.redirect('/');
    //res.render('pages/login.ejs')
})

app.post('/login', async function(req, res) {
        if(req.body.login === ''){
            //await passport.authenticate('local')
            res.redirect(307, '/dologin');
        }else if(req.body.signup === ''){
            let user = {
                email: req.body.username.toLowerCase(),
                password: req.body.password,
            } as user
            await db_user.create(user)
            res.redirect('/login');
        }
});

app.get('/admin', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        //let config = await db.getConfig()
        const user = req.user as user
        if (user.email.toLowerCase() === config.web.email) {
            let admin = true
            let indicators = await db_indicator.getAll()
            let timeframes = await db_timeframe.getAll()
            let markets = await db_market.get()
            let exchanges = await db_exchange.getAll()
            let config = {}
            res.render('pages/admin.ejs', {auth, admin, config, indicators, timeframes, markets, exchanges })
        } else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
})

app.post('/admin',
    async function(req, res) {
        let auth = req.isAuthenticated()
        if (auth) {

        } else {
            // not logged in
            res.redirect('/login');
        }
    }
)
// redirecting all calls for markets to the markets router file
app.use('/market', marketsRouter)

app.use('/user', usersRouter)

app.use('/account', accountsRouter)

app.use('/algo', algosRouter)

app.use('/timeframe', timeframesRouter)

app.use('/indicator', indicatorsRouter)

/* app.use('/backtest', backtestsRouter)

app.use('/livetest', livetestsRouter)
 */
app.use('/tests', testsRouter)


if (config.web.https){
    https.createServer({
        key: fs.readFileSync( './cert/server.key' ),
        cert: fs.readFileSync( './cert/server.pem' )
    }, app).listen(config.web.port)
    console.log('vostok (https) webpage is available on port ' + config.web.port + ' !')
}else{
    app.listen(config.web.port, config.web.ip, () =>
        console.log('vostok webpage is available on port ' + config.web.port + ' !')
    );    
}
