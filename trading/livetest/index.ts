import * as db_test from '../../db/tests';
import { complete_test_info } from '../../interfaces/index';
import child from 'child_process';
import * as db_test_logs from '../../db/test_logs';
import * as db_test_trade from '../../db/test_trade';
/* tools */
import { convertIndicator, fetchNewMarketData } from '../tools/indicators';
import { initiateTest, endTest } from '../tools/initiate';

/* trading loop */
import { tradeLoop } from '../strategies';

let debug = false

let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

process.on('message', async function(msg) {
    //console.log('Message from parent:', msg);
    if(msg.test_id){
        try{
            await initiateTest(msg.test_id, 'livetest', startStrategies)
        }catch(err){
            console.log(err.stack, 'error on livetest main()', msg.test_id)
        }
    }
    process.exit()
  });
  
export async function startStrategies(test: complete_test_info){
    // this fnc will determine the appropriate trading strategy and call small fncs every candles
    let first = true
    while(test.original.state === 'running'){ 
        test.original = await db_test.get(test.original.id)
        if(first){
            first = false
            test = await convertIndicator(test)
        }
        for(const [j, market] of test.markets.entries()){
            //test.markets[j].market_data = await getLastCandles(test.markets[j].market.sputnik_id, 60000, 1)
            //console.log(test.markets[j].market_data.length, test.markets[j].market_data[test.markets[j].market_data.length - 1][0] > Number(test.markets[test.index].last_trade_time))
            if(test.markets[j].market_data.length > 1 && Number(test.markets[j].market_data[test.markets[j].market_data.length - 1][0]) > Number(test.markets[test.index].last_trade_time)){
                test.candle.i = test.markets[j].market_data.length - 1
                test = await fetchNewMarketData(test)
                test.index = j
                //debug ? console.log('market: ', j, 'candle: ', test.candle.i) : false
                //trade fnc
                test = await tradeLoop(test)
                // stop livetest if reach stop datetime
                if(test.original.stop <= test.markets[j].market_data[test.markets[j].market_data.length - 1][0]){
                    debug ? console.log(test.original.id, 'stopping:', test.original.stop, test.markets[j].market_data[test.markets[j].market_data.length - 1][0]) : false
                    test.original.state = 'done' //await endTest(test)
                }
            }else if(test.markets[j].market_data.length > 1){
                debug ? console.log(test.original.id, 'sputnik data too old :', test.markets[j].market_data[test.markets[j].market_data.length - 1][0], 'needed : ', test.markets[test.index].last_trade_time, 'time :', new Date().valueOf()) : false
            }
        }
        await sleep(30000)
    }
    if (test.original.state === 'restart') {
        await db_test_logs.unlinkAll(test.original.id)
        await db_test_trade.unlinkChilds(test.original.id)
        await db_test.update(Number(test.original.id), 'init')
        try {
            let forked = child.fork(`./build/trading/${test.type}/index.js`)
            forked.on('message', (msg) => {
                console.log(`Message from ${test.type}er: "` + msg + '"')
            })
            forked.send({ test_id : test.original.id })
        } catch (err) {
            console.log(err.stack, 'forking test')
        }
    }else{
        debug ? console.log(test.original.id, 'ending', test.original.start) : false
        // candles ended, save a done side
        await endTest(test)
    }
   
    return 0
}
