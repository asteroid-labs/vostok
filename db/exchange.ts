const debug = false
import { pool } from './index';

export async function createFirstExchange(name: string) {
    let res = {} as any
    let text = 'SELECT * FROM exchanges WHERE name = $1'
    try {
        res = await pool.query(text, [name])
    } catch (err) {
        console.log(err.stack, 'error create first exchange')
    }
    if ((res.rows.length === 0)) {
        let text = 'INSERT INTO exchanges (name) VALUES ($1) ON CONFLICT DO NOTHING RETURNING ID;'
        try {
            res = await pool.query(text, [name]) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save exchange')
            }
            return res.rows[0]
        } catch (err) {
            console.log(err.stack, 'error exchange')
        }

    }else{
        return res.rows[0]
    }
}

export async function getAll() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM exchanges ORDER BY name ASC;`)
    } catch (err) {
        console.log(err.stack, 'error get exchanges')
    }
    return res.rows
}
