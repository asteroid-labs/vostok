import * as db_timeframe from '../timeframe'
import { timeframe } from '../../interfaces/index';

test('DB - Timeframe - Create, Find, Update, Unlink', async()=>{
  let data = {
    "id" : 42,
    "name": 'Testing',
    "msec_value": 1
  } as timeframe
  let result = await db_timeframe.create(data)
  data.id = result.id
  expect(result).toBeTruthy()
  // Created OK

  data.msec_value = data.msec_value.toString()
  expect(await db_timeframe.getOne(data.id)).toEqual(data)
  // Get OK

  data.name = 'Updated Testing'
  expect(await db_timeframe.update(data)).toBeTruthy()
  expect(await db_timeframe.getOne(data.id)).toEqual(data)
  // Update OK

  afterAll(async() => {
    expect(await db_timeframe.unlink(data.id)).toBeTruthy()
    // Unlink OK
  })
})
