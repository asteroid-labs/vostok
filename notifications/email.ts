import { message_info } from '../interfaces/index';
import * as db_user from '../db/user';

import nodemailer from 'nodemailer';
import * as config from '../config.json';
// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    pool: true,
    host: config.email.host,
    port: config.email.port,
    secure: true, // true for 465, false for other ports
    auth: {
      user: config.email.username,
      pass: config.email.password
    }
  });

const debug = false

export async function sendMailTrade(user_id: number, account_name: string, msg: message_info){
    let user = await db_user.getUser(user_id)
    let url = ''
    if(config.web.https){
        url = 'https://'+config.web.host+':'+config.web.port+'/'+msg.type+'/'+msg.test_id
    }else{
        url = 'http://'+config.web.host+':'+config.web.port+'/'+msg.type+'/'+msg.test_id
    }
    let message = {
        from: "vostok@asteroidlabs.io",
        to: user.email,
        subject: `Vostok ${msg.type} ${msg.side} trade`,
        text: "livetesting trade",
        html: `<h4>--- Binance / ${account_name} ---</h4><br/><p>${msg.side} trade: ${msg.qty} ${msg.quote} at ${msg.price} ${msg.base}.<br/>See the trading results live <a href="${url}">here!</a></p>`
    };
    await transporter.sendMail(message)
    return 'Done!'
}
