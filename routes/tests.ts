import express from 'express';
import * as db_algo from '../db/algo';
import * as db_account from '../db/account';
import * as config from '../config.json';
import * as db_test_trade from '../db/test_trade';
import * as db_test from '../db/tests';
import * as db_market from '../db/market';
import * as db_test_logs from '../db/test_logs';
import { calcInitial } from './tools';
import child from 'child_process';
import fetch from 'node-fetch';
import { user, test, market_test } from '../interfaces/index';

let router: express.Router = express.Router();

router.get('/:type', async function(req, res) {
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        let user = req.user as user
        let admin = false
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        let type = req.params.type
        let algos = await db_algo.getAllFromUser(user.id)
        for (const algo of algos) {
            algo.buy = JSON.stringify(algo.buy_pref)
            algo.sell = JSON.stringify(algo.sell_pref)
        }
        let markets = await db_market.get()
        let accounts = await db_account.getAll(user.id, false)
        let tests = await db_test.getByUser(user.id, type)

        for(const test of tests){
            let trades = await db_test_trade.getChilds(test.id)
            test.total = 0
            test.total_percent = 0

            test.completion = 0
            let latest_log = await db_test_logs.getLatest(test.id)
            if(typeof latest_log !== 'undefined'){
                //console.log(latest_log, Number(latest_log.test_time - test.start), Number(test.stop - test.start))
                test.completion = Number(latest_log.test_time - test.start) / Number(test.stop - test.start) * 100
                if(test.completion > 100){
                    test.completion = 100
                }else if(test.completion < 0){
                    test.completion = 0
                }
            }
            
            //let algo = await db_algo.getOne(user.id, test.account_id, test.algo_id, true)
            //let initial_base_amount = await calcInitial(test, algo, trades)
            /* for(const trade of trades){
                test.total = (trade.qty * trade.price) - (test.total > initial_base_amount ? test.total : initial_base_amount)
                test.total_percent = test.total/initial_base_amount*100
            } */
            if(trades.length > 0){
                /* let data = await db_test_trade.getTradeGains(test.id, trades[trades.length - 1].id as number)
                test.total = data.gain
                test.total_percent = data.percent */
                let inital_total = (trades[0].account_base_value + (trades[0].account_quote_value * trades[0].price))

                let trade_market = await db_market.getOne(trades[ trades.length - 1 ].pair_id)
                let end_base_name = trade_market.name.split("/")[1]
                if(end_base_name !== 'BTC'){
                    // get ticker for this market
                    inital_total = inital_total * trades[ trades.length - 1 ].price
                }

                test.total = ((trades[trades.length - 1].account_base_value + (trades[trades.length - 1].account_quote_value * trades[trades.length - 1].price)) - inital_total)
                test.total_percent = (test.total/inital_total*100)
            }
            
            
            let markets = []
            if(typeof test.pair_ids === 'object'){
                for(const pair of test.pair_ids){
                    let market_test = {market: await db_market.getOne(Number(pair))} as market_test
                    if(typeof market_test.market !== 'undefined'){
                        markets.push(market_test)
                    }
                }
            }else{
                let market_test = {market: await db_market.getOne(Number(test.pair_ids))} as market_test
                if(typeof market_test.market !== 'undefined'){
                    markets.push(market_test)
                }
            }
            
            test.market_display = markets.map(m => {return m.market.name}).join(", ").replace(/"([^"]+(?="))"/g, '$1')
        }
        res.render('pages/test.ejs', {auth, admin, algos, markets, accounts, tests, type })
    } else {
        res.redirect('/login');
    }
})


router.post('/:type', async function(req, res) {
    if (req.isAuthenticated()) {
        let user = req.user as user
        let type = req.params.type
        //console.log(typeof req.body.market, req.body.market)
        if(req.body.stop.length > 0){
            let market = ''
            if(typeof req.body.market === 'string'){
                market = JSON.stringify([Number(req.body.market)])
            }else{
                let markets = []
                for(let [i,m] of req.body.market.entries()){
                    markets.push((Number(m)))
                }
                market = JSON.stringify(markets)
            }

            let algo = await db_algo.getOneFromUser(user.id, Number(req.body.algo))
            let start_time = typeof req.body.start !== 'undefined' ? new Date(req.body.start).getTime() : new Date().getTime()
            if(type !== 'backtest'){
                start_time = start_time - (16 * 60000) // to start tests right away
            }
            let test = {
                'type': type,
                'time': Number(Date.now()),
                'user_id': user.id,
                'algo_id': algo.id,
                'exchange_id': 1,
                'pair_ids': market,
                'account_id': algo.account_id, 
                'base_amount': typeof req.body.amount !== 'undefined' ? Number(req.body.amount) : 0,
                'start': start_time,
                'stop': new Date(req.body.stop).getTime(),
                'state': 'init'
            } as test

            test = await db_test.create(test)
            // fork backtest with trade.id
            try {
                let forked = child.fork(`./build/trading/${type}/index.js`)
                forked.on('message', (msg) => {
                    console.log(`Message from ${type}er: "` + msg + '"')
                })
                forked.send({ test_id : test.id })
                // sending trade_id to child process for backtesting
                // save backtesting data to fake_trade table
            } catch (err) {
                console.log(err.stack, 'forking test')
            }
        }
        // redirect to backtests
        res.redirect(`/tests/${req.params.type}/`)
    } else {
        // not logged in
        res.redirect('/login');
    }
})

async function switchByState(id: number, user:user, type: string, action: string){
    let test = await db_test.get(Number(id))
    //console.log(id, test, action)
    if(user.id === test.user_id ){
        let flag = false
        switch (action) {
            case 'restart':
                // erase logs and trades ?
                if(test.state === 'init' || test.state === 'running' || test.state === 'stopping'){
                    // should be running, signaling only, fork will cleanup
                    await db_test.update(test.id, 'restart')
                }else{
                    // should not be running, restarting new fork  
                    // need to cleanup
                    await db_test_logs.unlinkAll(test.id)
                    //console.log('removed logs')
                    await db_test_trade.unlinkChilds(test.id)
                    //console.log('removed trades')
                    await db_test.update(test.id, 'init')
                    try {
                        let forked = child.fork(`./build/trading/${type}/index.js`)
                        forked.on('message', (msg) => {
                            console.log(`Message from ${type}er: "` + msg + '"')
                        })
                        forked.send({ test_id : test.id })
                        // sending trade_id to child process for backtesting
                        // save backtesting data to fake_trade table
                    } catch (err) {
                        console.log(err.stack, 'forking test')
                    }
                }
                break;

            case 'stop':
                await db_test.update(test.id, 'stopping')
                break;

            case 'delete':
                await db_test.update(test.id, 'deleted')
                break;
        }
    }
}

router.post('/state/:type', async function(req, res) {
    if (req.isAuthenticated()) {
        let user = req.user as user
        if(Array.isArray(req.body.checked)){
            for(const id of req.body.checked){
                switchByState(id, user, req.params.type, req.body.action)
            }
        }else{
            switchByState(req.body.checked, user, req.params.type, req.body.action)
        }
        // redirect to backtests
        res.redirect(`/tests/${req.params.type}/`)
    } else {
        // not logged in
        res.redirect('/login');
    }
})

router.get('/:type/:test_id', async function(req, res) {
    // retreive backtesting data (fake_trades)
    let auth = req.isAuthenticated()
    if (auth && req.user) {
        let user = req.user as user
        let type = req.params.type
        let admin = false
        if (user.email.toLowerCase() === config.web.email) {
            admin = true
        }
        let test = await db_test.get(Number(req.params.test_id))

        let algo = await db_algo.getOne(user.id, test.account_id, test.algo_id, true)
        let account = await db_account.getOne(user.id, test.account_id)
        let trades = await db_test_trade.getChilds(test.id)
        let initial_base_amount = await calcInitial(test, algo, trades)

        test.completion = 0
        let latest_log = await db_test_logs.getLatest(test.id)
        if(typeof latest_log !== 'undefined'){
            //console.log(latest_log, Number(latest_log.test_time - test.start), Number(test.stop - test.start))
            test.completion = Number(latest_log.test_time - test.start) / Number(test.stop - test.start) * 100
            if(test.completion > 100){
                test.completion = 100
            }else if(test.completion < 0){
                test.completion = 0
            }
        }
        
        let sputnik_ids = []
        for(const pair of test.pair_ids){
            sputnik_ids.push(await db_market.getOne(Number(pair)))
        }

        let market_datas = []
        if(user.id === test.user_id ){
            for(const sputnik_id of sputnik_ids){
                let sputnik_protocol = 'http'
                if (config.sputnik.https){
                    sputnik_protocol = 'https'
                }
                let jso = await fetch(`${sputnik_protocol}://${config.sputnik.host}:${config.sputnik.port}/api/markets_by_period_dynamic/${sputnik_id.sputnik_id}/${test.start}/${test.stop}`)
                market_datas.push({name: sputnik_id.name, id:sputnik_id.id, data:await jso.json()})
            }
            res.render(`pages/test.ejs`, {auth, admin, algo, account, market_datas, test, initial_base_amount, type})
        }else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
})

router.get('/:type/get_trades/:test_id', async function(req, res) {
    // retreive backtesting data (fake_trades)
    if (req.isAuthenticated() && req.user) {
        let user = req.user as user
        let test = await db_test.get(Number(req.params.test_id))
        let trades = await db_test_trade.getChilds(Number(req.params.test_id))
        for(const trade of trades){
            trade.market = await db_market.getOne(trade.pair_id)
           /*  let data = await db_test_trade.getTradeGains(test.id, trade.id as number)
            trade.total = data.gain
            trade.total_percent = data.percent */
        }
        let logs = await db_test_logs.getAll(Number(req.params.test_id))

        if(user.id === test.user_id ){
            res.json({trades: trades, state: test.state, logs: logs})
        } else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
})

export = router
