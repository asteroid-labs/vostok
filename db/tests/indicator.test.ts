import * as db_indicator from '../indicator'
import { indicator } from '../../interfaces/index';

test('DB - Indicator - Create, Find, Update, Unlink', async()=>{
  let data = {
    "id" : 42,
    "name": 'Testing',
    "function": 'test',
    "periods": []
  } as indicator
  let result = await db_indicator.create(data)
  data.id = result.id
  expect(result).toBeTruthy()
  // Created OK

  expect(await db_indicator.getOne(data.id as number)).toEqual(data)
  // Get OK

  data.name = 'Updated Testing'
  expect(await db_indicator.update(data)).toBeTruthy()
  expect(await db_indicator.getOne(data.id as number)).toEqual(data)
  // Update OK
  afterAll(async() => {
    expect(await db_indicator.unlink(data.id as number)).toBeTruthy()
    // Unlink OK
  })
})
