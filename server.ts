import * as db from './db/index';
import * as db_user from './db/user';
import * as db_exchange from './db/exchange';
import * as db_timeframe from './db/timeframe';
import * as db_indicator from './db/indicator';
import * as db_market from './db/market';
import * as config from './config.json'
import child from 'child_process';
import { user } from './interfaces/index';

async function main() {

    await db.checkTables()

    let user = {
        id: 1,
        name: 'Administrator',
        email: config.web.email.toLowerCase(),
        password: config.web.password,
    } as user

    try {
        await db_user.admin(user)
        await db_exchange.createFirstExchange('binance')
        await db_timeframe.createBaseTimeframes()
        await db_indicator.createBaseIndicators()
        await db_market.updateBaseMarkets()
    } catch (err) {
        console.log(err.stack, 'saving default values')
    }

    // Starting express router
    try {
        child.fork('./build/routes/index.js')
    } catch (err) {
        console.log(err.stack, 'forking router')
    }

    // Restarting dangling tests
    try {
        child.fork('./build/trading/dangling.js')
    } catch (err) {
        console.log(err.stack, 'forking dangling')
    }

    // Start user notifs
    try {
        let notif = child.fork('./build/notifications/users.js')
        notif.send({m: 'start'})
    } catch (err) {
        console.log(err.stack, 'forking users notifs')
    }
    // Start testing things
/*      try {
        let notif = child.fork('./build/test.js')
        notif.send({m: 'start'})
    } catch (err) {
        console.log(err.stack, 'forking testing')
    }  */
}

main()