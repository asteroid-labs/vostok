import * as db_market from '../../db/market';
import { market_candle } from '../../interfaces/index';

async function transformCandles(market_data_objects: market_candle[]){
    let market_data = market_data_objects.map(candle => { return [
        candle.timed, 
        candle.opening_price, 
        candle.highest_price,
        candle.lowest_price,
        candle.closing_price,
        candle.volume_base 
    ]})

    if(market_data.length > 1 && market_data[0][0] > market_data[market_data.length -1][0]){
        market_data.reverse()
    }
    // will always return latest value in last position
    return market_data
}

export async function getCandles(id: number, timeframe: number, start: number, stop: number) {
    let market_data_objects = await db_market.getMarketInTimeframePeriod(id, timeframe, start, stop)

    return await transformCandles(market_data_objects)
}

export async function getLastCandles(id: number, timeframe: number, amount: number) {
    let market_data_objects = await db_market.getMarketInTimeframeLast(id, timeframe, amount)

    return await transformCandles(market_data_objects)
}