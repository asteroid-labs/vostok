
CREATE TABLE users (
    id           SERIAL PRIMARY KEY,
    name         TEXT,
    email        TEXT,
    password     TEXT,
    verified     BOOLEAN,
    uid          TEXT,
    hidden       TEXT,
    prefs        JSONB
);

CREATE TABLE exchanges (
    id       SERIAL PRIMARY KEY,
    name     TEXT
);

CREATE TABLE markets (
    id       SERIAL PRIMARY KEY,
    name     TEXT,
    sputnik_id  INT
);

CREATE TABLE accounts (
    id            SERIAL PRIMARY KEY,
    name          TEXT,
    user_id       INTEGER REFERENCES users (id),
    exchange      INTEGER REFERENCES exchanges (id),
    api_key       TEXT,
    api_secret        TEXT,
    prefs         JSONB,
    trading_status TEXT
);

CREATE TABLE timeframes (
    id       SERIAL PRIMARY KEY,
    msec_value    BIGINT NOT NULL,
    name     TEXT
);

CREATE TABLE algos (
    id            SERIAL PRIMARY KEY,
    user_id       INTEGER REFERENCES users (id),
    account_id    INTEGER REFERENCES accounts (id),
    name          TEXT,
    markets_min_volume  INTEGER,
    status        TEXT,
    buy_pref       JSONB,
    sell_pref      JSONB
);

CREATE TABLE indicators (
    id       SERIAL PRIMARY KEY,
    name     TEXT,
    function      TEXT,
    periods     JSONB
);

CREATE TABLE tests (
    id          SERIAL PRIMARY KEY,
    type        TEXT,
    time        BIGINT NOT NULL,
    exchange_id    INTEGER REFERENCES exchanges (id),
    pair_ids     JSONB,
    user_id        INTEGER REFERENCES users (id),
    account_id     INTEGER REFERENCES accounts (id),
    algo_id        INTEGER REFERENCES algos (id),
    base_amount     DOUBLE PRECISION,
    state           TEXT,
    start           BIGINT,
    stop            BIGINT
);

CREATE TABLE test_trades (
    id          SERIAL PRIMARY KEY,
    test     INTEGER REFERENCES tests (id),
    pair_id INTEGER REFERENCES markets (id),
    time        BIGINT NOT NULL,
    qty     DOUBLE PRECISION,
    price           DOUBLE PRECISION,
    side            TEXT,
    account_base_value           DOUBLE PRECISION,
    account_quote_value           DOUBLE PRECISION
);

CREATE TABLE balances (
    test_id      INTEGER REFERENCES tests (id),
    symbol    TEXT,
    free     DOUBLE PRECISION,
    locked    DOUBLE PRECISION,
    CONSTRAINT unique_balances_con PRIMARY KEY (test_id, symbol)
);

CREATE TABLE real_balances (
    account_id      INTEGER REFERENCES accounts (id),
    symbol    TEXT,
    free     DOUBLE PRECISION,
    locked    DOUBLE PRECISION,
    CONSTRAINT unique_real_balances_con PRIMARY KEY (account_id, symbol)
);

CREATE TABLE test_logs(
    time            BIGINT NOT NULL,
    test_time            BIGINT NOT NULL,
    id   INTEGER REFERENCES tests (id),
    text   TEXT,
    type    TEXT,
    PRIMARY KEY(id, time)
);