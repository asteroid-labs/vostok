import { complete_test_info, algo_layers } from '../../interfaces/index';
import { MarketSellLayer } from '../layers/market';
import { write } from './logs';
import * as db_account from '../../db/account'
import { cancelLastSymbolOrder } from '../../binance';
const debug = false

export async function stall(test: complete_test_info, side: string, trigger: boolean, fnc: Function){
    //stall can only be activated on limit or trailing
    //console.log(test.markets[test.index].market_data[test.candle.i][0], test.markets[test.index].trade_info.state, layer.stall_protect.current, layer.stall)
    let layer = {} as algo_layers
    if(side === 'buy'){
        layer = test.algo.buy_pref.layers[test.algo.buy_pref.layer_index]
    }else{
        layer = test.algo.sell_pref.layers[test.algo.sell_pref.layer_index]
    }
    if(layer.stall_protect.active && layer.stall_protect.current <= 0){
        // active and triggered and not trailing, need to market sell or reset buy
        test = await triggered(test, side)
    }else{
        // stall protect is deactivated or not depleted, trying to trade
        test = await fnc(test, trigger)
    }

    return test
}

export async function triggered(test: complete_test_info, side: string){
    // active and triggered, need to market sell or reset buy
        // if algo stal_protect is initialy activated
    write(test.markets[test.index].market_data[test.candle.i][0],
        test.markets[test.index].market.name,
        'stall',
        test.original.id,
        debug,
        `: reseting ${side} stall protection after it depleted`
    ) 
    if(test.type === 'real'){
        /* cancel trade */
        let account = await db_account.getOne(test.original.user_id, test.original.account_id)
        // create new exhcange class
        await cancelLastSymbolOrder(account, test.markets[test.index].market, side)
    }

    if(side === 'sell'){
        //selling market
        write(test.markets[test.index].market_data[test.candle.i][0],
            test.markets[test.index].market.name,
            'stall',
            test.original.id,
            debug,
            `: ${side} Stall Protect is depleted (was ${test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall} minutes), selling all`
        )
        /* test.algo.sell_pref.layers.push({'type': 'market', 'percent': 0, 'price': 0, 'qty': 100, 'qty_percent': true, 'stall': 0, 'stall_protect': {'active': false, 'current': 0}})
        let temp_layer = test.algo.sell_pref.layer_index
        test.algo.sell_pref.layer_index = test.algo.sell_pref.layers.length - 1 */
        //console.log(test.algo.sell_pref.layer_index, test.algo.sell_pref.layers)
        test = await MarketSellLayer(test, true);
       /*  test.algo.sell_pref.layers.splice(-1, 1) // remove last element
        test.algo.sell_pref.layer_index = temp_layer // reset the layer index
        console.log(test.algo.sell_pref.layers, test.algo.sell_pref.layer_index) */
    }
    test.markets[test.index].trade_info.price = 0
    test = await reset(test, side)
    return test
}

export async function reset(test: complete_test_info, side: string){
    if(side === 'buy'){
        test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall_protect.current = test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall
        test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall_protect.active = false
        test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].price = 0
    }else{
        test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall_protect.current = test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall
        test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall_protect.active = false
        test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].price = 0
    }
    
    return test
}

export async function activate(test: complete_test_info, side: string){
    //console.log(layer.stall_protect.active, layer.stall_protect.current, layer.stall)
    if(side === 'buy'){
        if(!test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall_protect.active && test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall > 0){
            // if(test.markets[test.index].trade_info.state === 'sell'){console.log(test.markets[test.index].pref.layer_index, 'activating')}
            test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall_protect.current = test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall
            test.algo.buy_pref.layers[test.algo.buy_pref.layer_index].stall_protect.active = true
        }
    }else{ 
        if(!test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall_protect.active && test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall > 0){
            // if(test.markets[test.index].trade_info.state === 'sell'){console.log(test.markets[test.index].pref.layer_index, 'activating')}
            test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall_protect.current = test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall
            test.algo.sell_pref.layers[test.algo.sell_pref.layer_index].stall_protect.active = true
        }
    }
    return test
}